<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $table = 'articles';
    protected $fillable = [
        'title',
        'image',
        'details',
        'date_time'
    ];
    protected $guarded=[
      'created_by'
    ];

    public function createdBy(){
        return $this->belongsTo('App\User', 'created_by', 'id')
                    ->select('id', 'first_name', 'last_name');
    }
}
