<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataPoint extends Model
{
    protected $table = 'user_data_points';

    protected $fillable = [
      'user_id',
      'point_type',
      'point_value',
      'created_at'
    ];


    public static $pointTypes = [
      'image',
      'Body Weight',
      'Fat Mass',
      'Fat Percent',
      'Fat Free Mass',
      'Total Body Water'
    ];

    public function getPointValueAttribute($value){
      $point = json_decode($value, true);
      if(json_last_error() == JSON_ERROR_NONE){
        return $value;
      }
      return $point;
    }
}
