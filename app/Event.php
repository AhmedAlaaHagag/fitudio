<?php

namespace App;

use App\Setting;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $table = 'events';

    protected $fillable = [
      'image_id',
      'title',
      'description',
      'from',
      'to',
      'participant_fees',
      'subscriber_fees',
      'fees_currency',
      'address',
      'geo_location',
      'type',
      'lat',
      'lng',
      'is_featured',
      'created_by'
    ];

    protected $appends = ['totalFees'];

    public function image()
    {
      return $this->morphMany('App\Image', 'imageable');
    }

    public function users()
    {
      return $this->belongsToMany('App\Event', 'users_events', 'event_id', 'user_id')->withTimestamps();
    }

    public function members()
    {
      return $this->belongsToMany('App\User', 'users_events', 'event_id', 'user_id')
            ->select('users.id', 'first_name', 'last_name', 'type', 'city', 'lat', 'lng', 'email', 'phone', 'birth_date', 'height')
            ->withTimestamps();
    }

    public function payments()
    {
      return $this->morphMany('App\Payment', 'payable');
    }

    public function gettotalFeesAttribute()
    {
        $comission = Setting::whereKey('comission')->first();
        return $this->participant_fees + ($comission->value / $this->participant_fees * 100.0);
    }
}
