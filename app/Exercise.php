<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exercise extends Model
{
    protected $table = 'exercises';
    protected $fillable = [
        'name',
        'target_area',
        'muscle_id',
        'exercise_type_id',
        'equipment_needed',
        'level',
        'alternative_name',
    ];

    protected $appends = ['exercise_type', 'muscle'];
    protected $with = ['images'];

    public function images()
    {
        return $this->morphMany('App\Image', 'imageable');
    }

    public function exerciseType(){
        return $this->belongsTo(ExerciseType::class);
    }
    public function getExerciseTypeAttribute(){
        return $this->exerciseType()->first()->name;
    }

    public function muscle(){
        return $this->belongsTo(Muscle::class);
    }

    public function getMuscleAttribute(){
        return $this->muscle()->first()->name;
    }
}
