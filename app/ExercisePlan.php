<?php

namespace App;

use App\Traits\Notification;
use Illuminate\Database\Eloquent\Model;

class ExercisePlan extends Model {
	use Notification;
	protected $table = 'training_exercises';
	protected $fillable = [
		'name',
		'training_id',
		'exercise_id',
		'sets_no',
		'rest_time',
		'weight',
		'no_of_repeats',
		'day',
		'trainer_id',
	];

	public function exercise() {
		return $this->belongsTo(Exercise::class);
	}

	public function training() {
		return $this->belongsTo(Training::class);
	}

	public function plans() {
		return $this->morphToMany('App\TraineePlans', 'planable');
	}

}
