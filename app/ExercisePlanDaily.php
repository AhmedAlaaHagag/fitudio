<?php

namespace App;

use App\Traits\Notification;
use Illuminate\Database\Eloquent\Model;

class ExercisePlanDaily extends Model {
	protected $table = 'exercise_plans';
    protected $guarded = [];
}
