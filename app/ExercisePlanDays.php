<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExercisePlanDays extends Model
{
    protected $table = "exercise_plans_days";
    protected $fillable = ['exercises_plan_id', 'day'];
    protected $appends = ['exercises'];
    protected $hidden = ['id'];
    public $timestamps = false;

    public function getMealsAttribute()
    {
        return $this->exercises();
    }

    public function exercises()
    {
        return $this->hasMany(Exercise::class,'day_id')->get();
    }
}
