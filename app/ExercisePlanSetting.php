<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExercisePlanSetting extends Model
{
    protected $table = 'exercises_plan_settings';
    protected $fillable = ['exercises_id','plan_start','plan_end','week_start'];
    public $timestamps = false ;

}
