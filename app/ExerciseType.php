<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExerciseType extends Model
{
    protected $table = 'exercises_types';
    protected $fillable = [
        'name'
    ];
}
