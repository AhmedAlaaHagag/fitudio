<?php

namespace App;

use App\Traits\Notification;
use Illuminate\Database\Eloquent\Model;

class Follow extends Model
{
    use Notification;
    protected $table = 'follow_trainers';
    protected $fillable = ['trainer_id', 'trainee_id'];

    const UPDATED_AT = null;
}
