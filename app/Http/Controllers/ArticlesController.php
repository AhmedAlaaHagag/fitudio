<?php

namespace App\Http\Controllers;

use App\Article;
use Illuminate\Http\Request;

class ArticlesController extends Controller
{
    // articles list
    public function index(Request $request){
    	$article = Article::with('createdBy');
        if($request->input('featured')==true){
            return $article->where('featured',1)->get()->toArray();
        }
        return $article->get()->toArray();
    }


    public function show($id){
    	$article = Article::with('createdBy')->whereId($id)->first();
    	if($article){
    		return $article->toArray();
    	}

    	return response()->json(['message' => 'Article with id '.$id.' not found!'], 404);
    }
}
