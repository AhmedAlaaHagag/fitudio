<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\OauthClient;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Http\Exception\HttpResponseException;

class AuthController extends Controller
{
    /**
     * Handle a login request to the application.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function postLogin(Request $request)
    {
        try {
            $this->validatePostLoginRequest($request);
        } catch (HttpResponseException $e) {
            return $this->onBadRequest();
        }
        try {
            // Authenticate client app
            if (!$this->appAuth($request)) {
                return $this->unAuthenticated();
            }
            // Attempt to verify the credentials and create a token for the user
            else if (!$this->exists($request->email)) {
              return $this->notExist();
            }
            // uncomment to enable user activation
            else if (!$this->activated($request)) {
              return $this->inactive();
            }
            else if (!$token = JWTAuth::attempt(
                $this->getCredentials($request)
            )) {
                return $this->onUnauthorized();
            }
        } catch (JWTException $e) {
            // Something went wrong whilst attempting to encode the token
            return $this->onJwtGenerationError();
        }
        // All good so return the token and user data
        $user = User::with('images', 'dataPoints', 'medicals', 'specialities')
            ->whereEmail($request->email)
            ->first();

        return $this->onAuthorized($token, $user);
    }

    /**
     * Handle a login request to the application via social media.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function socialLogin(Request $request)
    {
        try {
            $this->validateSocialLoginRequest($request);
        } catch (HttpResponseException $e) {
            return $this->onBadRequest();
        }

        try {
            // Authenticate client app
            if (!$this->appAuth($request)) {
                return $this->unAuthenticated();
            }

            $user = User::with('images', 'dataPoints', 'medicals', 'specialities')
                ->whereSocialProviderId($request->social_provider_id)
                ->first();
            if ($user) {
                // login user via social media
                $user->update([
                    'social_provider_token' => $request->social_provider_token,
                    'social_provider_type' => $request->social_provider_type
                ]);
            } else {
                // register user with social media, then login
                $this->register($user, $request);
            }
        } catch (JWTException $e) {
            // Something went wrong whilst attempting to encode the token
            return $this->onJwtGenerationError();
        }
        // All good so return the token and user data
        $token = JWTAuth::fromUser($user);
        return $this->onAuthorized($token, $user);
    }

    /**
     * Validate authentication request.
     *
     * @param  Request $request
     * @return void
     * @throws HttpResponseException
     */
    protected function validatePostLoginRequest(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|max:255',
            'password' => 'required',
        ]);
    }

    /**
     * Validate authentication request.
     *
     * @param  Request $request
     * @return void
     * @throws HttpResponseException
     */
    protected function validateSocialLoginRequest(Request $request)
    {
        $this->validate($request, [
            'social_provider_id' => 'required|numeric',
            'social_provider_token' => 'required',
            'social_provider_type' => 'required'
        ]);
    }

    /**
     * What response should be returned on bad request.
     *
     * @return JsonResponse
     */
    protected function onBadRequest()
    {
        return new JsonResponse([
            'message' => 'invalid_credentials'
        ], Response::HTTP_BAD_REQUEST);
    }

    /**
     * What response should be returned on invalid credentials.
     *
     * @return JsonResponse
     */
    protected function onUnauthorized()
    {
        return new JsonResponse([
            'message' => 'invalid_credentials'
        ], Response::HTTP_UNAUTHORIZED);
    }

    /**
     * What response should be returned on error while generate JWT.
     *
     * @return JsonResponse
     */
    protected function onJwtGenerationError()
    {
        return new JsonResponse([
            'message' => 'could_not_create_token'
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * What response should be returned on authorized.
     *
     * @return JsonResponse
     */
    protected function onAuthorized($token, $user)
    {
        return new JsonResponse([
            'message' => 'token_generated',
            'data' => [
                'token' => $token,
            ],
            'user' => $user
        ]);
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    protected function getCredentials(Request $request)
    {
        $credentials = $request->only('email', 'password');
        // uncomment to enable user activation
        if ($request->enforce === 0) {
            $credentials['activated'] = 1;
        }
        return $credentials;
    }

    /**
     * Check if user account activated or not.
     *
     * @param string $email
     *
     * @return boolean
     */
    protected function activated($request)
    {
        if (User::whereEmail($request->email)->first()->activated === 1) {
            return true;
        } else if ($request->enforce) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * What response should be returned if user is inactivated.
     *
     * @return JSON
     */
    protected function inactive()
    {
      return new JsonResponse(['message' => 'inactivated account']);
    }

    /**
     * Check if user email exists.
     *
     * @param string $email
     *
     * @return boolean
     */
    protected function exists($email)
    {
      if(User::whereEmail($email)->first()) return true;
      else return false;
    }

    /**
     * What response should be returned if user email does not exist.
     *
     * @return JSON
     */
    protected function notExist()
    {
      return new JsonResponse(['message' => 'this email does not exist'], Response::HTTP_NOT_FOUND);
    }

    /**
     * Check if app is authenticated.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return boolean
     */
    protected function appAuth($request)
    {
        $client = OauthClient::wherePasswordClient(1)->first();
        $client_id = $client->id;
        $client_secret = $client->secret;

        if ($request['grant_type'] == 'password' && $request['client_id'] == $client_id && $request['client_secret'] == $client_secret) {
            return true;
        }
        return false;
    }

    /**
     * What response should be returned if app is unauthenticated.
     *
     * @return JSON
     */
    protected function unAuthenticated()
    {
        return new JsonResponse(['message' => 'unauthenticated app'], Response::HTTP_UNAUTHORIZED);
    }

    /**
     * Register new user.
     *
     * @param JSON $user
     * @param \Illuminate\Http\Request $request
     *
     * @return void
     */
    protected function register($user, $request)
    {
        $activation_code = str_random(30);
        $input=$request->except(
            'profile_image_id',
            'activated',
            'activation_code',
            'created_at',
            'updated_at'
        );
        $input['activation_code'] = $activation_code;
        $input['password'] = app('hash')->make($request->password);
        $user = User::create($input);
        // send verification mail
        Mail::send('email.verify', ['activation_code' => $activation_code], function ($message) use ($input) {
            $message->to($input['email'], $input['name'])->subject('Verify your account!.');
        });
    }

    /**
     * Invalidate a token.
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteInvalidate()
    {
        $token = JWTAuth::parseToken();

        $token->invalidate();

        return new JsonResponse(['message' => 'token_invalidated']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\Response
     */
    public function patchRefresh()
    {
        $token = JWTAuth::parseToken();

        $newToken = $token->refresh();

        return new JsonResponse([
            'message' => 'token_refreshed',
            'data' => [
                'token' => $newToken
            ]
        ]);
    }

    /**
     * Get authenticated user.
     *
     * @return \Illuminate\Http\Response
     */
    public function getUser()
    {
        return new JsonResponse([
            'message' => 'authenticated_user',
            'data' => JWTAuth::parseToken()->authenticate()
        ]);
    }
}
