<?php 

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Http\Exception\HttpResponseException;

class AuthController extends Controller
{
	function login(Request $request){


		$grant_type = $request->grant_type;
		if($grant_type == 'password'){
			try{
				$this->validateLoginRequest($request);
			}
			catch (HttpResponseException $e) {
	            return $this->onBadRequest();
	        }
            $response = $this->usernamePasswordLogin($request);
            if($response->status() != Response::HTTP_OK){
                return $response;
            }
            $token = $response->getData()->token;
            $user = User::with('images', 'dataPoints', 'medicals', 'specialities')
                ->whereEmail($request->email)
                ->first();
            return $this->onAuthorized($token, $user);
		}elseif ($grant_type == 'social') {
			try{	
				$this->validateSocialLoginRequest($request);
			}
			catch (HttpResponseException $e) {
	            return $this->onBadRequest();
	        }
            $token = $this->socialLogin($request)->getData()->token;
            $user = User::with('images', 'dataPoints', 'medicals', 'specialities')
                ->whereEmail($request->email)
                ->first();

            return $this->onAuthorized($token, $user);
		}else{
			return $this->onBadRequest();
		}
	}

    /**
     * What response should be returned on authorized.
     *
     * @return JsonResponse
     */
    protected function onAuthorized($token, $user)
    {

        return new JsonResponse([
            'message' => 'token_generated',
            'data' => [
                'token' => $token,
            ],
            'user' => $user
        ]);
    }
	protected function socialLogin($request){
		$user = User::whereSocialProviderId($request->social_provider_id)->first();
        if ($user) {
            // login user via social media
            $user->update([
                'social_provider_token' => $request->social_provider_token,
                'social_provider_type' => $request->social_provider_type
            ]);
            $token = JWTAuth::fromUser($user);
			if(!$token){
				return new JsonResponse(['message' => 'invalid_credentials'], Response::HTTP_UNAUTHORIZED);
			}
			return new JsonResponse(['token' => $token]);
        }else{
        	return $this->onBadRequest();
        }
	}


	protected function usernamePasswordLogin($request){
		if(!$this->exists($request)){
			return new JsonResponse(['message' => 'this email does not exist'], Response::HTTP_NOT_FOUND);
		}
		if(!$this->activated($request)){
			return new JsonResponse(['message' => 'inactivated account'], Response::HTTP_UNAUTHORIZED);
		}

		$token = $this->generateToken($request);
		if(!$token){
			return new JsonResponse(['message' => 'invalid_credentials'], Response::HTTP_UNAUTHORIZED);
		}
		return new JsonResponse(['token' => $token], Response::HTTP_OK);
	}


	protected function validateLoginRequest(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|max:255',
            'password' => 'required',
        ]);
    }

    protected function validateSocialLoginRequest(Request $request)
    {
        $this->validate($request, [
            'social_provider_id' => 'required|numeric',
            'social_provider_token' => 'required',
            'social_provider_type' => 'required'
        ]);
    }


	protected function authenticate($request){

	}

	protected function activated($request){
		$user = User::whereEmail($request->email)->first();
		return $user->activated === 1 ? true : false;
	}

	protected function exists($request){
		$user = User::whereEmail($request->email)->first();
		return $user ? true : false;
	}


	protected function onBadRequest()
    {
        return new JsonResponse([
            'message' => 'invalid_credentials'
        ], Response::HTTP_BAD_REQUEST);
    }


    protected function onUnauthorized()
    {
        return new JsonResponse([
            'message' => 'invalid_credentials'
        ], Response::HTTP_UNAUTHORIZED);
    }


    protected function generateToken($request){
    	$credentials = ['email' => $request->email, 'password' => $request->password];
    	$token = JWTAuth::attempt($credentials);
    	return $token ? $token : false;
    }
}
?>