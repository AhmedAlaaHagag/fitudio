<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\DataPoint;
use App\Image;
use App\User;
use Auth;
use DB;

class DataPointController extends Controller
{
    public function index(Request $request)
    {

      $dataPoints = DataPoint::where('point_type','!=','image')
          ->join('users', 'users.id', 'user_id')
          ->select('users.id as user_id', 'first_name', 'last_name', 'email', 'phone', 'user_data_points.*');
      if ($request['trainee_id'] != null) {
          $dataPoints->whereuserId($request->trainee_id);
      }
      if ($request['from']) {
          $dataPoints->where('user_data_points.created_at', '>=', $request['from']);
      }
      if ($request['to']) {
          $dataPoints->where('user_data_points.created_at', '<=', $request['to']);
      }
      if ($request['point_type']) {
          $dataPoints->where('user_data_points.point_type', 'like', '%'.$request['point_type'].'%');
      }
      $dataPoints = $dataPoints->get();
      $responseArray=[];
      foreach ($dataPoints as $dataPoint){
          if($dataPoint->point_type=='Body Weight'){
            $createdAt = new Carbon($dataPoint->created_at);
            $createdAt = $createdAt->timestamp;
            $responseBodyWeightData['value'] = $dataPoint->point_value;
            $responseBodyWeightData['time'] = $createdAt;
            $reposeBodyWeight[] = $responseBodyWeightData;
          }
          if($dataPoint->point_type=='Fat Mass'){
              $createdAt = new Carbon($dataPoint->created_at);
              $createdAt = $createdAt->timestamp;
              $responseFatMassData['value'] = $dataPoint->point_value;
              $responseFatMassData['time'] = $createdAt;
              $reposeFatMass[] = $responseFatMassData;
          }
          if($dataPoint->point_type=='Fat Percent'){
              $createdAt = new Carbon($dataPoint->created_at);
              $createdAt = $createdAt->timestamp;
              $responseFatPercentData['value'] = $dataPoint->point_value;
              $responseFatPercentData['time'] = $createdAt;
              $reposeFatPercent[] = $responseFatPercentData;
          }
          if($dataPoint->point_type=='Fat Free Mass'){
              $createdAt = new Carbon($dataPoint->created_at);
              $createdAt = $createdAt->timestamp;
              $responseFatFreeData['value'] = $dataPoint->point_value;
              $responseFatFreeData['time'] = $createdAt;
              $reposeFatFree[] = $responseFatFreeData;
          }
          if($dataPoint->point_type=='Total Body Water'){
              $createdAt = new Carbon($dataPoint->created_at);
              $createdAt = $createdAt->timestamp;
              $responseTotalBodyWaterData['value'] = $dataPoint->point_value;
              $responseTotalBodyWaterData['time'] = $createdAt;
              $responseTotalBodyWater[] = $responseTotalBodyWaterData;
          }
      }

        if(!empty($reposeBodyWeight)){
            $reposeBodyWeightA=array();
            $reposeBodyWeightA['type']='Body Weight';
            $reposeBodyWeightA['data']=$reposeBodyWeight;
            $responseArray['points'][] =$reposeBodyWeightA;
        }
        if(!empty($reposeFatMass)){
            $reposeFatMassA=array();
            $reposeFatMassA['type']='Fat Mass';
            $reposeFatMassA['data']=$reposeFatMass;
            $responseArray['points'][] =$reposeFatMassA;
        }
        if(!empty($reposeFatPercent)){
            $reposeFatPercentA=array();
            $reposeFatPercentA['type']='Fat Percent';
            $reposeFatPercentA['data']=$reposeFatPercent;
            $responseArray['points'][] =$reposeFatPercentA;
        }
        if(!empty($reposeFatFree)){
            $reposeFatFreeA=array();
            $reposeFatFreeA['type']='Fat Free Mass';
            $reposeFatFreeA['data']=$reposeFatFree;
            $responseArray['points'][] =$reposeFatFreeA;
        }
        if(!empty($responseTotalBodyWater)){
            $responseTotalBodyWaterA=array();
            $responseTotalBodyWaterA['type']='Total Body Water';
            $responseTotalBodyWaterA['data']=$responseTotalBodyWater;
            $responseArray['points'][] = $responseTotalBodyWaterA;
        }
        return $responseArray;
    }

    public function show($user_id)
    {
        // we refere to data point as a reading
        // 
        $dataPoints = DataPoint::select('point_type', 'point_value' , 'created_at')
                                ->whereUserId($user_id)
                                ->where('point_type', '!=', 'image')
                                ->get();

        $pointTypes = DataPoint::$pointTypes;

        $points = [];
        foreach ($pointTypes as $point) {
          $points[$point] = $dataPoints->filter(function($value, $key) use($point){
            return $value->point_type == $point;
          });
        }

        $data = ['week' => [], 'month' => [], 'year' => []];
        // get weeks
        $startOfWeek = Carbon::now()->startOfWeek();
        foreach ($points as $key => $readings) {
          foreach ($readings as $reading) {
            if($reading->created_at->gte($startOfWeek)){
              $data['week'][$key][] = $reading;
            }
          }
        }

        // get month
        $startOfMonth = Carbon::now()->startOfMonth();
        foreach ($points as $key => $readings) {
          foreach ($readings as $reading) {
            if($reading->created_at->gte($startOfMonth)){
              $data['month'][$key][] = $reading;
            }
          }
        }

        // get year
        $startOfYear = Carbon::now()->startOfYear();
        $pointsPerMonth = [];
        foreach ($points as $key => $readings) {
          foreach ($readings as $reading) {
            if($reading->created_at->gte($startOfYear)){
              $month = $reading->created_at->format('m');
              $pointsPerMonth[$key][$month][] = $reading;
            }
          }
        }

        $yearPoints = [];
        foreach ($pointsPerMonth as $pointName => $points) {
          foreach ($points as $month => $readings) {
            $count = count($readings);
            $sum = 0;
            foreach ($readings as $reading) {
              $sum += $reading->point_value;
            }
            $avg = round($sum / $count, 2);
            $data['year'][$pointName][$month] = [
              'point_value' => $avg,
              'point_type' => $pointName,
              'create_at' => date('Y').'-'.$month.'-1 00:00:00'
            ];
          }
        }

        return response()->json($data, 200);
    }

    public function store(Request $request)
    {
      $dataPoints = $request->all();
      $userId = \Auth::user()->id;
      // validate data points
      foreach (DataPoint::$pointTypes as $type) {
        if($type == 'image'){
          if(count($dataPoints['image']) > 3 ){
            return response()->json(['message' => 'you can upload up to 3 images only'], 422);
          }
          continue;
        }
      }
      DB::beginTransaction();
      try{
        foreach ($dataPoints as $type => $value) {
              if($type == 'image' && !empty($dataPoints['image'] )){
                    $images = [];
                      foreach ($dataPoints['image'] as $img) {
                          if(!empty($img)){
                              $filename = 'data-point-'.time().'.jpg';
                              $file = base64_to_jpeg($img, base_path('/public/images').'/'.$filename);
                              $images[] = url('/images').'/'.$filename;
                          }
                      }
                      if(count($images)>0){
                          $value = json_encode($images);
                      }
                      if(!empty($value[0])){
                          $point = new DataPoint(['user_id' => $userId, 'point_type' => $type, 'point_value' => $value]);
                          $point->save();
                      }
                }else{
                  foreach ($dataPoints['points'] as $point){
                      $point = new DataPoint(['user_id' => $userId, 'point_type' => $point['type'], 'point_value' => $point['value']]);
                      $point->save();
                  }
                }
        }
        DB::commit();
      }catch(\Exception $e){
        DB::rollback();
        return response()->json(['message' => $e->getMessage()], 500);
      }
      return response()->json(['message' => 'ok']);
    }
}
