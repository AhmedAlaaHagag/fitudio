<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Event;
use App\Image;
use App\User;
use DB;

class EventController extends Controller
{
    public function index(Request $request)
    {
      return Event::with('image', 'members')
        ->where('title', 'like', '%'.$request['keyword'].'%')
        ->orwhere('from', 'like', '%'.$request['keyword'].'%')
        ->orwhere('to', 'like', '%'.$request['keyword'].'%')
        ->orwhere('fees_currency', 'like', '%'.$request['keyword'].'%')
        ->orwhere('address', 'like', '%'.$request['keyword'].'%')
        ->orwhere('type', 'like', '%'.$request['keyword'].'%')
        ->orwhere('created_by', 'like', '%'.$request['keyword'].'%')
        ->get()->toArray();
    }

    public function show($id)
    {
      $event = Event::with('image', 'members')
        ->where('events.id', $id)
        ->leftjoin('images', 'images.id', 'image_id')
        ->select('events.*', 'images.image_url', 'images.image_name')
        ->first();
      if($event) return $event->toArray();
      else return ['status_code' => 404, 'message' => 'event not found 404!'];
    }

    public function store(Request $request)
    {
      $validation = $this->validate($request, [
        'title' => 'required',
        'participant_fees' => 'required|numeric',
        'subscriber_fees' => 'numeric',
        'type' => 'in:group,gym',
        'from' => 'required|date',
        'to' => 'required|date',
        'lat' => 'numeric',
        'lng' => 'numeric',
      ]);

      $event = Event::create($request->all());
      // upload event cover image
      if ($request->has('image')) {
          $filename = 'event-'.round(microtime(true) * 1000).'.jpg';
          $file = base64_to_jpeg($request['image'], base_path('/public/images').'/'.$filename);

        $img = new Image();
        $img->image_url = url('/images').'/'.$filename;
        $img->image_name = $filename;
        $event->image()->save($img);
        $event->update(['image_id' => $img->id]);
      }
      if ($event) return $event->toArray();
      else return ['status_code' => 500, 'message' => 'server error, could not create event!'];
    }

    public function update(Request $request, $id)
    {
      $validation = $this->validate($request, [
        'participant_fees' => 'numeric',
        'subscriber_fees' => 'numeric',
        'type' => 'in:group,gym',
        'from' => 'date',
        'to' => 'date',
        'lat' => 'numeric',
        'lng' => 'numeric',
      ]);

      $event = Event::find($id);
      if($event){
        $flag = $event->update($request->all());
        if($request->has('image')){
            $filename = 'event-'.round(microtime(true) * 1000).'.jpg';
            $file = base64_to_jpeg($request['image'], base_path('/public/images').'/'.$filename);

          $img = new Image();
          $img->image_url = url('/images').'/'.$filename;
          $img->image_name = $filename;
          $event->image()->save($img);
          $event->update(['image_id' => $img->id]);
        }
        if ($flag) return $event->toArray();
        else return ['status_code' => 500, 'message' => 'server error, could not update event!'];
      } else {
        return ['status_code' => 404, 'message' => 'event not found 404!'];
      }
    }

    public function destroy($id)
    {
      $event = Event::find($id);
      if ($event) {
        $flag = $event->delete();
        if ($flag) return ['status_code' => 200];
        else return ['status_code' => 500, 'message' => 'server error, could not delete event!'];
      } else {
        return ['status_code' => 404, 'message' => 'event not found 404!'];
      }
    }

    public function upload_image(Request $request, $event_id)
    {
      $event = Event::find($event_id);
      if($request->has('image') && $event){
          $filename = 'event-'.round(microtime(true) * 1000).'.jpg';
          $file = base64_to_jpeg($request['image'], base_path('/public/images').'/'.$filename);
        // attach the new image to images table
        $image = new Image();
        $image->image_url = url('/images') . '/' . $filename;
        $image->image_name = $filename;
        $event->image()->save($image);
        $event->update(['image_id' => $image->id]);
        return $image->toArray();
      }
      else if ($event == null) return ['status_code' => 404, 'message' => 'event not found 404!'];
      else if (!$request->hasFile('image')) return ['status_code' => 404, 'message' => 'you must upload an image!'];
    }

    public function delete_image($event_id)
    {
      $event = Event::find($event_id);
      $img = ($event) ? Image::find($event->image_id) : null;
      if($img && $event){
        // delete image file
        unlink(base_path().'/public/images/'.$img->image_name);
        // delete image in DB
        $img->delete();
        $event->update(['image_id' => null]); // reset image_id field
        return ['status_code' => 200, 'message' => 'OK'];
      }
      else if ($event == null) return ['status_code' => 404, 'message' => 'event not found 404!'];
      else if ($img == null) return ['status_code' => 404, 'message' => 'this event has no cover image!'];
    }

    public function register_event(Request $request)
    {
      $this->validate($request, [
          'user_id' => 'required|integer',
          'event_id' => 'required|integer',
      ]);

      $user = User::find($request->user_id);
      $event = Event::find($request->event_id);
      if($user && $event){
        // ensure that user registers this event once
        foreach ($user->events as $event) {
          if ($event->id == $request->event_id) return ['msg' => 'already registered'];
        }
        $user->events()->attach($request->event_id);
        $event = Event::find($request->event_id);
        // send confirmation email
        Mail::send('email.confirm_event', ['user' => $user, 'event' => $event], function ($message) use ($user){
          $message->to($user->email, $user->name)->subject('Event registration');
        });
        return ['status_code' => 200, 'message' => 'OK'];
      }
      else if ($user == null) return ['status_code' => 404, 'message' => 'user not found 404!'];
      else if ($event == null) return ['status_code' => 404, 'message' => 'event not found 404!'];
    }

    public function cancel_event(Request $request)
    {
      $user = User::find($request->user_id);
      $event = Event::find($request->event_id);
      $registration = DB::table('users_events')
        ->where('user_id', $request->user_id)
        ->where('event_id', $request->event_id)
        ->first();

      if ($user && $event && $registration) {
          $user->events()->detach($request->event_id);
          return ['status_code' => 200, 'message' => 'OK'];
      }
      else if ($registration == null) {
          return ['status_code' => 404, 'message' => 'user is not registered to this event!'];
      }
      else if ($user == null) {
          return ['status_code' => 404, 'message' => 'user not found 404!'];
      }
      else if ($event == null) {
          return ['status_code' => 404, 'message' => 'event not found 404!'];
      }
    }

    public function near_by(Request $request)
    {
      $this->validate($request, [
          'lat' => 'numeric',
          'lng' => 'numeric',
      ]);

      $lat = $request->lat;
      $lng = $request->lng;
      $distance = $request->distance;
      return DB::table('events')
        ->leftjoin('images', 'images.id', 'image_id')
        ->select(DB::raw('images.image_url, images.image_name, events.*, 111.045* DEGREES(ACOS(COS(RADIANS('.$lat.')) * COS(RADIANS(lat)) * COS(RADIANS('.$lng.') - RADIANS(lng)) + SIN(RADIANS('.$lat.')) * SIN(RADIANS(lat)))) AS distance_in_km'))
        ->having('distance_in_km', '<', $distance)
        ->orderby('distance_in_km')
        ->get();
    }
}
