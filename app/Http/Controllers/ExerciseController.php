<?php

namespace App\Http\Controllers;

use App\User;
use App\Image;
use App\Exercise;
use Illuminate\Http\Request;

class ExerciseController extends Controller
{
    public function index(Request $request)
    {
      $exercise =  Exercise::with('images');
      if($request->has('filter')){
          $exercise->where('name', 'like', '%'.$request->filter.'%')
              ->orWhere('created_at', 'like', '%'.$request->filter.'%')
              ->orWhere('alternative_name', 'like', '%'.$request->filter.'%')
              ->orWhere('target_area', 'like', '%'.$request->filter.'%')
              ->orWhere('level', 'like', '%'.$request->filter.'%');
      }
      $exercise = $exercise->get()->toArray();
      return $exercise;

    }
    public function getExerciseByMusclesGroups($id){
        $exercise =  Exercise::with('images')->where('muscle_id',$id)->get()->toArray();
        return $exercise;
    }
    public function show($id)
    {
      return Exercise::with('images')->whereId($id)->first()->toArray();
    }

    public function store(Request $request)
    {
      $validation = $this->validate($request, [
        'main_image' => 'required',
      ]);

        $input_values = $request->only([   'name',
            'target_area',
            'muscle_id',
            'exercise_type_id',
            'equipment_needed',
            'level',
            'alternative_name',]);
        $exercise = Exercise::create($input_values);

        $this->AttachImageToExrcise($request, $exercise);
        return $exercise->toArray();


    }

    public function update(Request $request, $id)
    {
      if($exercise = Exercise::find($id)){
        $exercise->update($request->except('main_image', 'created_by'));

          $this->AttachImageToExrcise($request, $exercise);
        return $exercise->toArray();
      }
      else return ['status_code' => 404, 'message' => 'exercise not found 404'];
    }

    public function destroy($id)
    {
      if($exercise = Exercise::find($id)){
        $exercise->update(['main_image' => null]); // drop foreign key to delete images
        foreach ($exercise->images()->get() as $img) { // delete images attached to exercise
          unlink(base_path().'/public/images/'.$img->image_name);
          $img->delete();
        }
        $exercise->delete();
        return ['status_code' => 200, 'message' => 'OK'];
      }
      else return ['status_code' => 404, 'message' => 'exercise not found 404'];
    }

    public function exerciseImage(Request $request, $id)
    {
      $validation = $this->validate($request, [
        'main_image' => 'required|integer'
      ]);
      $img = Image::find($request->main_image);

      if($img && ($exercise = Exercise::find($id))){
        $exercise->update(['main_image' => $request->main_image]);
        return $exercise->toArray();
      }
      else if($img == null) return ['status_code' => 404, 'message' => 'image not found 404'];
      else return ['status_code' => 404, 'message' => 'exercise not found 404'];
    }

    public function deleteImage(Request $request, $id)
    {
        $this->validate($request, [
            'exercise_id' => 'required|integer'
        ]);

        $img = Image::find($id);
        $exercise = Exercise::find($request->exercise_id);
        $cover = $exercise->main_image;

        if ($img && $cover != $id) {
            unlink(base_path('/public/images/').$img->image_name);
            $img->delete();
            return ['status_code' => 200, 'message' => 'OK'];
        } else if ($cover == $id) {
            return ['status_code' => 412, 'message' => 'this image is a cover for '.$exercise->title.' exercise!'];
        } else if ($img == null) {
            return ['status_code' => 404, 'message' => 'image not found 404!'];
        }
    }

    /**
     * @param Request $request
     * @param $exercise
     */
    private function AttachImageToExrcise(Request $request, $exercise)
    {
        if ($request->has('main_image')) {
            $filename = 'exercise-' . round(microtime(true) * 1000) . '.jpg';
            $file = base64_to_jpeg($request->main_image, base_path('/public/images') . '/' . $filename);
            // store in DB
            $img = new Image();
            $img->image_url = url('/images') . '/' . $filename;
            $img->image_name = $filename;
            $exercise->images()->save($img);
            $exercise->update(['main_image' => $img->id]);
        }
        if ($request->has('images')) {
            $files = $request->get('images');
            foreach ($files as $key => $file) {

                $filename = 'exercise-' . round(microtime(true) * 1000) . '.jpg';
                $file = base64_to_jpeg($file, base_path('/public/images') . '/' . $filename);
                // store in DB
                $img = new Image();
                $img->image_url = url('/images') . '/' . $filename;
                $img->image_name = $filename;
                $exercise->images()->save($img);
            }
        }
    }
}
