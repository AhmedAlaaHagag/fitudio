<?php

namespace App\Http\Controllers;

use App\ExercisePlanDaily;
use App\ExercisePlanSetting;
use App\NutritionPlanSetting;
use App\NutritionsPlan;
use App\TraineePlans;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\ExercisePlan;
use App\Training;
use App\Exercise;
use App\User;
use Auth;

use Illuminate\Support\Facades\DB;


class ExercisePlanController extends Controller
{
    public function index(Request $request)
    {
        $user_id = \Auth::user()->id;
        $exercisesPlan = ExercisePlanDaily::where('trainer_id', $user_id);
        $nutrition_plans = NutritionsPlan::where('trainer_id', $user_id);
        if ($request->has('filter')) {
            $exercisesPlan->where('name', 'LIKE', "%" . $request->filter . "%");
            $nutrition_plans->where('name', 'LIKE', "%" . $request->filter . "%");
        }

        $exercisesPlan = $exercisesPlan->paginate(config('app.pagination_value'));
        $nutrition_plans = $nutrition_plans->paginate(config('app.pagination_value'));

        return ['exercises_plan' => $exercisesPlan->toArray()['data'], 'nutrition_plans' => $nutrition_plans->toArray()['data']];
    }

    public function show($training_id)
    {
        return ExercisePlan::with('exercise')->whereTrainingId($training_id)->get()->toArray();
    }


    public function getByDay($day)
    {
        $user = \Auth::user();
        return ExercisePlan::with('exercise')
            ->select('training_exercises.*')
            ->join('trainings', 'trainings.id', '=', 'training_exercises.training_id')
            ->where('day', $day)
            ->get()
            ->toArray();
    }

    public function store(Request $request)
    {
        $validation = $this->validate($request, [
            'training_id' => 'required|integer',
            'exercise_id' => 'required|integer',
            'day' => 'date'
        ]);

        $training = Training::find($request->training_id);
        $exercise = Exercise::find($request->exercise_id);
        if ($training && $exercise) {
            // send notification to trainee
            $trainer = User::find($training->trainer_id);
            $trainee = User::find($training->trainee_id);
            ExercisePlan::notify($trainee->device_id_token, 'New exercise planCoach ' . $trainer->name . ' has added a new exercise plan for you.', 'exercise_plan');
            $planInputs = $request->all();
            $planInputs['trainer_id'] = \Auth::user()->id;
            return ExercisePlan::create($planInputs)->toArray();
        } else if ($training == null) return ['status_code' => 404, 'message' => 'training not found 404'];
        else if ($exercise == null) return ['status_code' => 404, 'message' => 'exercise not found 404'];
    }

    public function update(Request $request, $id)
    {
        $validation = $this->validate($request, [
            'training_id' => 'integer',
            'exercise_id' => 'integer',
            'day' => 'date',
        ]);

        $training = Training::find($request->training_id);
        $exercise = Exercise::find($request->exercise_id);
        $plan = ExercisePlan::find($id);
        if ($plan && $training && $exercise) {
            $plan->update($request->all());
            // send notification to trainee
            $trainer = User::find($training->trainer_id);
            $trainee = User::find($training->trainee_id);
            ExercisePlan::notify($trainee->device_id_token, 'Updated exercise plan', 'Coach ' . $trainer->name . ' has updated an exercise plan for you.', 'exercise_plan');

            return $plan->toArray();
        } else if ($plan == null) return ['status_code' => 404, 'message' => 'exercise plan not found 404!'];
        else if ($training == null) return ['status_code' => 404, 'message' => 'training not found 404'];
        else if ($exercise == null) return ['status_code' => 404, 'message' => 'exercise not found 404'];
    }

    public function destroy($id)
    {
        if ($plan = ExercisePlan::find($id)) {
            $plan->delete();
            return ['status_code' => 200];
        } else return ['status_code' => 404, 'message' => 'exercise plan not found 404'];
    }

    // accepts training_id and day to be checked and notified for
    public function reminder($training_id, $day)
    {
        $excercise_plans = ExercisePlan::whereTrainingId($training_id)->get();
        $training = Training::find($training_id);
        $flag = false;
        foreach ($excercise_plans as $plan) {
            if ($day == $plan->day) // there is a plan for that day
                $flag = true;
        }
        if (!$flag) {
            $trainer = User::find($training->trainer_id);
            $trainee = User::find($training->trainee_id);
            ExercisePlan::notify($trainer->device_id_token, 'Exercise plan reminder', 'You have to set exercise plan for day ' . $day . ' for Mr.' . $trainee->name, 'exercise_plan'); // (token, title, body)
        }
    }

    public function exercisePlansSetting(Request $request)
    {


        $this->validate($request, [
                'trainee_id' => 'required',
                'plans_setting' => 'array'
            ]
        );

        try {
            $save = DB::transaction(function () use ($request) {
                foreach ($request->plans_setting as $item) {
                    $item['start_date'] = Carbon::createFromFormat('Y-d-d', $item['start_date']);
                    $item['end_date'] = Carbon::createFromFormat('Y-d-d', $item['end_date']);
                    if ($item['plan_type'] == 'exercise') {
                        $item = array_add($item, 'exercise_id', $item['plan_id']);
                        ExercisePlanSetting::create($item);
                    } elseif ($item['plan_type'] == 'nutrition') {
                        $item = array_add($item, 'nutrition_id', $item['plan_id']);
                        NutritionPlanSetting::create($item);
                    }
                }
            });
        } catch (\Exception $exception) {

            $save = $exception->getMessage();
        }

        return $save ? ['status_code' => 201] : ['status_code' => 401, 'error' => $save];
    }


    public function sendPlan(Request $request)
    {
        $this->validate($request, [
            'trainee_id' => 'required|exists:users,id',
            'plan' => 'required|array'
        ]);

        $save = DB::transaction(function () use ($request) {
            $saved = 0;
            foreach ($request->plan as $plan) {
                if ($plan['type'] == 'exercise') {
                    $create = TraineePlans::create(['plan_id' => $plan['id'], 'trainee_id' => $request->trainee_id, "planable_type" => ExercisePlan::class, "planable_id" => $plan['id']]);
                    !$create ?: $saved++;
                }elseif($plan['type'] == 'nutrition'){
                    $create = TraineePlans::create(['plan_id' => $plan['id'], 'trainee_id' => $request->trainee_id, "planable_type" => NutritionsPlan::class, "planable_id" => $plan['id']]);
                    !$create ?: $saved++;
                }
            }
            return $saved;
        });
        //todo send notification to trainee

        return $save ? ['status_code' => 201,'send_plans'=>$save] : ['status_code' => 401, "message" => $save];

    }

}
