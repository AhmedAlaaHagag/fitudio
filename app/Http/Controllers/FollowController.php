<?php

namespace App\Http\Controllers;

use DB;
use App\User;
use App\Follow;
use App\Trainer;
use App\Trainee;
use Illuminate\Http\Request;

class FollowController extends Controller
{
    // show trainer followers
    public function index(Request $request)
    {
      $this->validate($request, [
         'trainer_id' => 'required|integer'
     ]);

      if($trainer = Trainer::find($request->trainer_id)){ // check if trainee exists
        return $trainer->with(['followers' => function ($query){
            $query->join('users as follow', 'follow.id', 'trainee_id')
            ->leftjoin('images', 'images.id', 'follow.profile_image_id')
            ->select('follow.id', 'trainer_id', 'trainee_id', 'follow.name', 'follow.email', 'follow.body_type', 'follow.profile_image_id', 'images.image_url')
            ->get();
          }])
          ->whereId($request->trainer_id)
          ->select('id', 'first_name', 'last_name', 'email')
          ->get()->toArray();
      }
      else return ['status_code' => 404, 'message' => 'trainer not found 404!'];
    }

    // show trainee followings
    public function show(Request $request, $trainee_id)
    {
      if($trainee = Trainee::find($trainee_id)){ // check if trainee exists
        return $trainee->with(['followings' => function ($query){
            $query->join('users as follow', 'follow.id', 'trainer_id')
            ->leftjoin('images', 'images.id', 'follow.profile_image_id')
            ->select('follow.id', 'trainer_id', 'trainee_id', 'follow.name', 'follow.email', 'follow.profile_image_id', 'images.image_url')
            ->get();
          }])
          ->whereId($trainee_id)
          ->select('id', 'first_name', 'last_name', 'email')
          ->get()->toArray();
      }
      else return ['status_code' => 404, 'message' => 'trainee not found 404!'];
    }

    // trainee follow trainer
    public function store(Request $request)
    {
      $this->validate($request, [
          'trainer_id' => 'required|integer',
          'trainee_id' => 'required|integer',
      ]);

      if(($trainer = Trainer::find($request->trainer_id)) && ($trainee = User::find($request->trainee_id))){ // check if trainer exists
        foreach ($trainer->followers as $follower) { // check if trainee aleardy following this trainer
          if($follower->trainee_id == $request->trainee_id) return ['message' => 'already_following'];
        }
        $flag = Follow::create($request->all());
        if($flag){
          $trainee = User::find($request->trainee_id);
          Follow::notify($trainee->device_id_token, 'Notification', 'You have followed trainer successfully!','follow'); // (token, title, body) notification to trainee
          Follow::notify($trainer->device_id_token, 'Notification', $trainee->name.' has followed you.','follow'); // (token, title, body) notification to trainer
          return ['status_code' => 200, 'message' => 'OK'];
        } else return ['status_code' => 500];
      }
      else if($trainer == null) return ['status_code' => 404, 'message' => 'trainer not found 404!']; // trainer not found 404
      else if($trainee == null) return ['status_code' => 404, 'message' => 'trainee not found 404!']; // trainee not found 404
    }

    // accepts follow id
    public function destroy($id)
    {
      if($follow = Follow::find($id)){ // check if following relationship exists
        $follow->delete();
        return ['status_code' => 200, 'message' => 'OK'];
      } else{
        return ['status_code' => 404, 'message' => 'follow not found 404!'];
      }
    }
}
