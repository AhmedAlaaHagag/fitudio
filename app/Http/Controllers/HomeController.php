<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    private $ArticleController=null;
    private $EventController=null;
    private $TrainerController=null;

    function __construct()
    {
        $this->ArticleController=new ArticlesController();
        $this->EventController=new EventController();
        $this->TrainerController=new TrainerController();
    }

    public function index(){ // Home Page
        $homePage=[];
        $articlesRequest = new Request();
        $articlesRequest->request->add(['featured' => true]);
        $articles = $this->ArticleController->index($articlesRequest);
        $events = $this->EventController->index(new Request());
        $trainers= $this->TrainerController->index(new Request());
        $homePage['articles'] = $articles;
        $homePage['events'] = $events;
        $homePage['trainers'] = $trainers;
        return $homePage;
    }
}
