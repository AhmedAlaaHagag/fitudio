<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Storage;
use App\User;
use App\Image;

class ImageController extends Controller
{
    public function store(Request $request, $user_id)
    {
      $validation = $this->validate($request, [
          'image' => 'required',
      ]);

      $user = User::find($user_id);
      if($request->has('image') && $user){
        $filename = 'avatar-'.round(microtime(true) * 1000).'.jpg';
        $file = base64_to_jpeg($request['image'], base_path('/public/images').'/'.$filename);
        // attach the new image to images table
        $image = new Image();
        $image->image_url = url('/images') . '/' . $filename;
        $image->image_name = $filename;
        $user->images()->save($image);
        $user->update(['profile_image_id' => $image->id]);
        return $image->toArray();
    } else if($user == null) return ['status_code' => 404, 'message' => 'user not found 404!'];
      else if(!$request->hasFile('image')) return ['status_code' => 404, 'message' => 'you must upload an image!'];
    }

    public function destroy(Request $request, $id)
    {
      $img = Image::find($id);
      if($img){
        // delete image file
        unlink(base_path().'/public/images/'.$img->image_name);
        // delete image in DB
        $img->delete();
        return ['status_code' => 200, 'message' => 'OK'];
      } else{
        return ['status_code' => 404, 'message' => 'image not found 404!'];
      }
    }
}
