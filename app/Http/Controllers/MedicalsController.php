<?php

namespace App\Http\Controllers;

use App\Medical;
use App\Trainee;
use Illuminate\Http\Request;

class MedicalsController extends Controller
{
    public function index()
    {
        return Medical::with(['user' => function ($query) {
            $query->select('id', 'first_name', 'last_name', 'email', 'phone');
        }])->get()->toArray();
    }

    public function show($id)
    {
        $medical = Medical::with(['user' => function ($query) {
            $query->select('id', 'first_name', 'last_name', 'email', 'phone');
        }])->whereId($id)->first();
        if ($medical) {
            return $medical->toArray();
        } else {
            return ['status_code' => 404, 'message' => 'medical history not found 404!'];
        }
    }

    public function store(Request $request)
    {
        foreach ($request->all() as $req) {
            $trainee = Trainee::find($req['user_id']);
            if ($trainee) {
                Medical::create($req);
            } else {
                return ['status_code' => 404, 'message' => 'trainee with id '.$req['user_id'].' not found 404!'];
            }
        }
        return ['status_code' => 200, 'message' => 'OK'];
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'confirm' => 'integer'
        ]);

        $medical = Medical::find($id);
        if ($medical) {
            $medical->update($request->except('user_id'));
            return $medical->toArray();
        } else {
            return ['status_code' => 404, 'message' => 'medical history not found 404!'];
        }
    }

    public function destroy($id)
    {
        $medical = Medical::find($id);
        if ($medical) {
            $medical->delete();
            return ['status_code' => 200, 'message' => 'OK'];
        } else {
            return ['status_code' => 404, 'message' => 'medical history not found 404!'];
        }
    }

    /**
    * Returns enums for issue_type field in medical_histories table
    *
    * @param
    *
    * @return array of strings
    */
    public function enums()
    {
        $enums = getEnumValues('medical_histories', 'issue_type');
        return $enums;
    }
}
