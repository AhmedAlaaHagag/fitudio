<?php

namespace App\Http\Controllers;

use App\Medical;
use App\Muscle;
use App\Trainee;
use Illuminate\Http\Request;

class MuscleController extends Controller
{
    public function index()
    {
        return Muscle::get()->toArray();
    }

}
