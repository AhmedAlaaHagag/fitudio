<?php

namespace App\Http\Controllers;

use App\Notification;
use Illuminate\Http\Request;

class NotificationsController extends Controller
{

    public function index()
    {
        $user_id = \Auth::user()->id;
        return Notification::where('user_id', $user_id)->orderBy('updated_at', 'desc')->limit(100)->get();

    }

    /**
     * Return notifications for one user
     *
     * @param int $user_id
     *
     * @return JSON
     */
    public function show(Request $request, $user_id)
    {
        $this->validate($request, [
            'from' => 'date',
            'to' => 'date'
        ]);

        $notifications = Notification::whereUserId($user_id);

        if ($request['from']) {
            $notifications->where('created_at', '>=', $request['from']);
        }
        if ($request['to']) {
            $notifications->where('created_at', '<=', $request['to']);
        }

        return $notifications
            ->orderBy('created_at', 'desc')
            ->get()
            ->toArray();
    }

    /**
     * Mark notification as read or unread
     *
     * @param Illuminate\Http\Request $request , int $id
     *
     * @return JSON
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, ['read' => 'required|boolean']);

        $notification = Notification::find($id);

        if ($notification) {
            $notification->update(['read' => $request->read]);
            return $notification->toArray();
        } else {
            return ['status_code' => 404, 'message' => 'notification not found 404!'];
        }
    }

    /**
     * Delete notifications from/to sepicific dates
     *
     * @param Illuminate\Http\Request $request , int $user_id
     *
     * @return JSON
     */
    public function destroy(Request $request, $id)
    {


        $notifications = Notification::destroy($id);
        if ($notifications) {
            return ['status_code' => 200, 'message' => 'OK'];
        }
        return ['status_code' => 400, 'message' => 'Can\'t find notification with this id'];


    }

    /**
     * Returns number of unread notifications
     *
     * @param int $user_id
     * @return JSON
     */
    public function count($user_id)
    {
        $notifications = Notification::whereUserId($user_id)
            ->where('read', 0)
            ->count();

        return [
            'notifications' => $notifications
        ];
    }
}
