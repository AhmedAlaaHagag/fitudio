<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TrainingNutrition;
use App\Supplement;
use App\Nutrition;
use App\Training;
use App\User;

class NutritionController extends Controller
{
    public function index(Request $request)
    {
        return Nutrition::select('NDB_No', 'Shrt_Desc', 'Energ_Kcal')->where('Shrt_Desc', 'like', '%' . $request['name'] . '%')->get(); // all nutritions (filtered)
    }


    function getFoods(Request $request)
    {
        return $nutirations = Nutrition::select('NDB_No', 'Shrt_Desc', 'Energ_Kcal')->where('Shrt_Desc', 'like', '%' . $request['name'] . '%')->get()->toArray(); // all nutritions (filtered)
    }

    function getSupplements(Request $request)
    {
        return $supplements = Supplement::select('id', 'name', 'description', 'supplement_type_id')->where('name', 'like', '%' . $request['name'] . '%')->get()->toArray();
    }

    public function getNutrition($id)
    {
        $nutrition = Nutrition::where('NDB_No', $id)->first()->toArray(); // all nutritions (filtered)
        $sizes = [$nutrition['GmWt_Desc1'], $nutrition['GmWt_Desc2']];
        $nutrition['sizes'] = $sizes;
        return $nutrition;
    }

    public function show($training_id)
    {
        return TrainingNutrition::whereTrainingId($training_id)->get()->toArray();
    }

    // trainer create nutrition plan
    public function store(Request $request)
    {
        $validation = $this->validate($request, [
            'training_id' => 'required|integer',
            'nutrition_fact_NDB_no' => 'required',
            'day' => 'date'
        ]);

        $training = Training::find($request->training_id);
        $nutrition = Nutrition::where('NDB_No', $request->nutrition_fact_NDB_no)->first();
        if ($training && $nutrition) {
            // send notification to trainee
            $trainee = User::find($training->trainee_id);
            $trainer = User::find($training->trainer_id);
            $plan = TrainingNutrition::create($request->all())->toArray(); // cretae training nutrition if training exsits
            TrainingNutrition::notify($trainee->device_id_token, 'Coach ' . $trainer->name . ' has added a new nutrition plan for you.', json_encode($plan), 'nutrition_plan'); // (token, title, body)
            return $plan;
        } else if ($training == null) return ['status_code' => 404, 'message' => 'training not found 404!'];
        else if ($nutrition == null) return ['status_code' => 404, 'message' => 'nutrition fact not found 404!'];
    }

    // trainer update nutrition plan
    public function update(Request $request, $id)
    {
        $validation = $this->validate($request, [
            'training_id' => 'integer',
            'day' => 'date',
        ]);

        $nutrition_fact = Nutrition::where('NDB_No', $request['nutrition_fact_NDB_no'])->first();
        if ($nutrition = TrainingNutrition::find($id)) {
            if ($request['nutrition_fact_NDB_no'])
                if ($nutrition_fact == null) return ['status_code' => 404, 'message' => 'nutritiotn fact not found 404!'];
            $nutrition->update($request->all());
            // send notification to trainee
            $training = Training::find($nutrition->training_id);
            $trainer = User::find($training->trainer_id);
            $trainee = User::find($training->trainee_id);
            TrainingNutrition::notify($trainee->device_id_token, 'Nutrition plan updated', 'Coach ' . $trainer->name . ' has updated a nutrition plan for you.', 'nutrition_plan'); // (token, title, body)
            return $nutrition->toArray();
        } else return ['status_code' => 404, 'message' => 'nutrition plan not found 404!'];
    }

    // requires nutrition plan id
    public function destroy($id)
    {
        if ($nutrition = TrainingNutrition::find($id)) {
            $nutrition->delete();
            return ['status_code' => 200];
        } else return ['status_code' => 404, 'message' => 'nutritiotn plan not found 404!'];
    }

    // accepts training_id and day to be checked and notified for
    public function reminder($training_id, $day)
    {
        $nutrition_plans = TrainingNutrition::whereTrainingId($training_id)->get();
        $training = Training::find($training_id);
        $flag = false;
        foreach ($nutrition_plans as $plan) {
            if ($day == $plan->day) // there is a plan for that day
                $flag = true;
        }
        if (!$flag) {
            $trainer = User::find($training->$trainer_id);
            $trainee = User::find($training->$trainee_id);
            TrainingNutrition::notify($trainer->device_id_token, 'Nutrition plan reminder', 'You have to set nutrition plan for day ' . $day . ' for Mr.' . $trainee->name, 'nutrition_plan'); // (token, title, body)
        }
    }
}
