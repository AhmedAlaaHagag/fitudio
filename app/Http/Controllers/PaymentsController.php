<?php

namespace App\Http\Controllers;

use App\Payment;
use Illuminate\Http\Request;

class PaymentsController extends Controller
{
    public function show(Request $request, $id)
    {
        $this->validate($request, [
            'from' => 'date',
            'to' => 'date'
        ]);

        $payments = Payment::whereTrainerId($id);

        if ($request['from']) {
            $payments->where('created_at', '>=', $request['from']);
        }
        if ($request['to']) {
            $payments->where('created_at', '<=', $request['to']);
        }

        return $payments->get()->toArray();
    }

    public function destroy($id){
        $payment = Payment::where('id',$id)->first();
        $payment->delete();
        return ['status_code' => 200, 'message' => 'OK'];
    }


}
