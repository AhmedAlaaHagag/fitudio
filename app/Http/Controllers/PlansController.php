<?php

namespace App\Http\Controllers;

use App\ExercisePlanDaily;
use App\NutritionPlanDays;
use App\NutritionPlanMeals;
use App\NutritionsPlan;
use App\TrainingNutrition;
use App\ExercisePlan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\ExercisePlanDays;

class PlansController extends Controller
{

    function show($id)
    {

        $daysArray = array_merge(ExercisePlan::distinct()->get(['day'])->toArray(), TrainingNutrition::distinct()->get(['day'])->toArray());
        $plans = [];
        $days = [];
        foreach ($daysArray as $day) {
            $days[] = $day['day'];
        }
        $days = array_unique($days);
        foreach ($days as $day) {
            $date = new Carbon($day);
            $date = $date->timestamp;
            $plan['id'] = "";
            $plan['time'] = $date;
            $plan['type'] = "";
            $plan['name'] = "";
            $plans[] = $plan;
            $exercises = ExercisePlan::with('exercise')
                ->join('trainings', 'trainings.id', '=', 'training_exercises.training_id')
                ->where('trainings.trainee_id', $id)
                ->where('day', $day)
                ->get();
            $nutritions = TrainingNutrition::join('trainings', 'trainings.id', '=', 'training_nutrition.training_id')
                ->where('trainings.trainee_id', $id)
                ->where('day', $day)
                ->get();
            if (!$exercises->isEmpty() || !$nutritions->isEmpty()) {
                foreach ($exercises as $exercise) {
                    $date = new Carbon($exercise->day);
                    $date = $date->timestamp;
                    $plan['id'] = $exercise->id;
                    $plan['time'] = $date;
                    $plan['type'] = 'Exercise';
                    $plan['name'] = $exercise->exercise->name;
                    $plans[] = $plan;
                }
                foreach ($nutritions as $nutrition) {
                    $date = new Carbon($nutrition->day);
                    $date = $date->timestamp;
                    $plan['id'] = $nutrition->id;
                    $plan['time'] = $date;
                    $plan['type'] = 'Nutrition';
                    $plan['name'] = $nutrition->nutrition_description;
                    $plans[] = $plan;
                }
            }
        }

        return $plans;
    }

    public function getPlan($type, $id)
    {

        if ($type == 'nutrition') {
            $file = 'Nutrition plan.json';
        } else {
            $file = 'Exercise plan.json';
        }
        $contents = file_get_contents('../public/files/' . $file);
        return $contents;
    }


    public function store($type, Request $request)
    {
        if ($type == 'nutirations') {
            return $this->addNutritionPlan($request);
        } else {
            return $this->addExercisePlan($request);
        }
    }

    private function addExercisePlan($request)
    {
        $this->validate($request, [
            'days' => "required|array"
        ]);
        try {
                $trainer_id = \Auth::user()->id;
                $plan = ExercisePlanDaily::create(
                    ['name' => $request->name, 'recommendation' => $request->recommendation, 'trainer_id' => $trainer_id]);
                foreach ($request->days as $day) {
                    $day_name = $day['day_name'];
                    $planDay = ExercisePlanDays::create
                    (['day' => (string)$day_name, 'exercise_plan_id' => $plan->id]);
                    foreach ($day['exercises'] as $exercise) {
                        ExercisePlan::create(
                            [   'exercise_id' => $exercise['id'],
                                'sets_no' => $exercise['sets_no'],
                                'rest_time' => $exercise['rest_time'],
                                'weight' =>$exercise['weight'],
                                'no_of_repeats' => $exercise['no_of_repeats'],
                                'day_id' => $planDay->id,
                            ]);
                    }
                }
            return ['message' => 'success'];
        } catch (\Exception $exception) {
            return ['error' => $exception->getMessage()];
        }
    }

    private function addNutritionPlan($request)
    {

        $this->validate($request, [
            'days' => "required|array",
            "name" => "required"
        ]);

        try {
            $save = \DB::transaction(function () use ($request) {
                $trainer_id = \Auth::user()->id;
                $plan = NutritionsPlan::create(['name' => $request->name, 'recommendation' => $request->recommendation, 'trainer_id' => $trainer_id]);
                foreach ($request->days as $day) {
                    $day_name = $day['day_name'];
                    $PlanDay = NutritionPlanDays::create(['day' => $day_name, 'nutrition_plan_id' => $plan->id]);
                    foreach ($day['meals'] as $meal) {
                        $meals_food = $meal['food'];
                        $meals_supplement = $meal['supplement'];
                        $is_free = $meal['is_free'];
                        foreach ($meals_food as $food) {
                            if ($food) {
                                NutritionPlanMeals::create(
                                    ['NDB_No' => $food['NDB_No'],
                                        'size' => $food['size'],
                                        'number_of_serving' => $food['number_of_serving'],
                                        'day_id' => $PlanDay->id,
                                        'is_free' => $is_free,
                                        'type' => 'food',
                                    ]);
                            }
                        }
                        foreach ($meals_supplement as $supplement) {
                            if ($supplement) {
                                NutritionPlanMeals::create([
                                    'supplement_id' => $supplement['id'],
                                    'day_id' => $PlanDay->id,
                                    'type' => 'supplement',
                                    'is_free' => $is_free
                                ]);
                            }
                        }
                    }
                }
                return $plan;
            });
        } catch (\Exception $exception) {
            return ['error' => $exception->getMessage()];
        }
        return $save ? ['message' => 'success', 'plan' => $save] : ['error' => 'Some thing wrong on saving'];
    }
}
