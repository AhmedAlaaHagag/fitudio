<?php

namespace App\Http\Controllers;

use App\Trainer;
use Illuminate\Http\Request;
use App\User;
use App\Rate;

class RateController extends Controller
{
    public function show($trainer_id)
    {
      $rates = User::with('rates')
        ->whereId($trainer_id)
        ->select('id', 'first_name', 'last_name', 'email', 'phone', 'city')
        ->first();
      return $rates->toArray();
    }

    public function store(Request $request)
    {
      $this->validate($request, [
          'trainer_id' => 'required|integer',
          'trainee_id' => 'required|integer',
          'rate' => 'required|numeric',
      ]);

      $trainer = User::find($request->trainer_id);
      $trainee = User::find($request->trainee_id);
      if(!$trainer || !$trainee){
        return ['status_code' => 404, 'message' => 'trainee or trainer not found 404!'];
      }
      if($trainer->rates()->where('trainee_id', $request->trainee_id)->first()){
          $trainer->rates()->where('trainee_id', $request->trainee_id)->first()->update(['rate' => $request->rate]);
          return ['status_code' => 200, 'message' => 'OK'];
      }
      else {
          $rate = Rate::create($request->all());
            $trainer->notify($trainer->device_id_token,$trainee->first_name." has given you a $request->rate rating for your training",$rate,'rating');
          if($rate) return ['status_code' => 200, 'message' => 'OK'];
          else return ['status_code' => 500, 'message' => 'server error, could not create rate!'];
      }
    }

    public function update(Request $request, $id)
    {
      $this->validate($request, [
          'trainer_id' => 'integer',
          'trainee_id' => 'integer',
          'rate' => 'numeric',
      ]);

      $trainer = User::find($request->trainer_id);
      $trainee = User::find($request->trainee_id);
      $rating = Rate::find($id);
      if($trainer && $trainee && $rating){
        $rate = Rate::find($id)->update($request->all());
        if($rate) return ['status_code' => 200, 'message' => 'OK'];
        else return ['status_code' => 500, 'message' => 'server error, could not update rate!'];
      }
      else if($rating == null) return ['status_code' => 404, 'message' => 'rate not found 404!'];
      else if($trainer == null) return ['status_code' => 404, 'message' => 'trainer not found 404!'];
      else if($trainee == null) return ['status_code' => 404, 'message' => 'trainee not found 404!'];
    }

    public function destroy($id)
    {
      $rate = Rate::find($id);
      if($rate){
        $rate->delete();
        return ['status_code' => 200, 'message' => 'OK'];
      } else{
        return ['status_code' => 404, 'message' => 'rate not found 404!'];
      }
    }
}
