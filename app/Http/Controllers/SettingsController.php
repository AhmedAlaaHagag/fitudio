<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    public function index()
    {
        $settings = Setting::all();
        return $settings->toArray();
    }

    public function show($id)
    {
        $setting = Setting::find($id);

        if ($setting) {
            return $setting->toArray();
        } else {
            return ['status_code' => 404, 'message' => 'setting not found 404!'];
        }
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'key' => 'required',
            'value' => 'required'
        ]);

        $setting = Setting::create($request->all());
        return $setting->toArray();
    }

    public function update(Request $request, $id)
    {
        $setting = Setting::find($id);

        if ($setting) {
            $setting->update($request->all());
            return $setting->toArray();
        } else {
            return ['status_code' => 404, 'message' => 'setting not found 404!'];
        }
    }

    public function destroy($id)
    {
        $setting = Setting::find($id);

        if ($setting) {
            $setting->delete();
            return ['status_code' => 200, 'message' => 'OK'];
        } else {
            return ['status_code' => 404, 'message' => 'setting not found 404!'];
        }
    }
}
