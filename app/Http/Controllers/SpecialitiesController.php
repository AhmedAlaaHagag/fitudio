<?php

namespace App\Http\Controllers;

use App\Speciality;
use Illuminate\Http\Request;

class SpecialitiesController extends Controller
{
    public function index()
    {
        return Speciality::all();
    }

    public function show($id)
    {
        $speciality = Speciality::find($id);
        if ($speciality) {
            return $speciality->toArray();
        } else {
            return ['status_code' => 404, 'message' => 'speciality not found 404!'];
        }
    }

    public function store(Request $request)
    {
        $this->validate($request, ['speciality' => 'required']);

        return Speciality::create(['speciality' => $request->speciality])->toArray();
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, ['speciality' => 'required']);

        $speciality = Speciality::find($id);

        if ($speciality) {
            $speciality->update(['speciality' => $request->speciality]);
            return $speciality->toArray();
        } else {
            return ['status_code' => 404, 'message' => 'speciality not found 404!'];
        }
    }

    public function destroy($id)
    {
        $speciality = Speciality::find($id);

        if ($speciality) {
            $speciality->delete();
            return ['status_code' => 200, 'message' => 'OK'];
        } else {
            return ['status_code' => 404, 'message' => 'speciality not found 404!'];
        }
    }
}
