<?php

namespace App\Http\Controllers;

use App\User;
use Dingo\Api\Auth\Auth;
use Stripe;
use App\Event;
use App\Payment;
use App\Training;
use Illuminate\Http\Request;

class StripeController extends Controller
{
    public function show($id) // Stripe checkout demo for developing purposes
    {
        $trainings = Training::whereTraineeId($id)
            ->wherePaid(0)
            ->whereStatus('approved')
            ->get();
        return view('checkout', compact('trainings'));
    }

    public function checkout(Request $request)
    {

      /*  $this->validate($request, [
            'stripeToken' => 'required',
            'model_id' => 'required|integer',
            'model_type' => 'required',
            'amount' => 'required|numeric',
            'stripeEmail' => 'email'
        ]);*/

/*        Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));*/

        // charge checkout
       /* $charge = Stripe\Charge::create(array(
            "amount" => $request->amount,
            "currency" => "egp",
            "description" => "Fitudio charge",
            "source" => $request->stripeToken,
        ));*/
        $user = \Auth::user()->id;
        $payment = new Payment();
        if ($request->model_type == 'training') {
            $model = Training::find($request->model_id);
            $model->update(['paid' => 1]);
            $payment->trainer_id = $model->trainer_id;
        } else if ($request->model_type == 'event') {
            $model = Event::find($request->model_id);
        }
        $user = User::find($user)->first();
        $payment->amount = $request->amount;
        $payment->email = $user->email;
        $payment->charge = 1;

        $model->payments()->save($payment);

        return [
            'status_code' => 200,
            'message' => 'OK'
        ];
    }

    public function refund(Request $request)
    {
        $this->validate($request, [
            'amount' => 'numeric',
            'charge' => 'required'
        ]);

        Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        Stripe\Refund::create(array(
          "charge" => $request->charge,
          "amount" => $request->amount,
        ));
        $payment = Payment::whereCharge($request->charge)->first();
        $payment->delete();

        return ['status_code' => 200, 'message' => 'OK'];
    }
}
