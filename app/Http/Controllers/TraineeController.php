<?php

/**
 * TraineeController uses App\User with type trainee as for handling Trainees
 */

namespace App\Http\Controllers;

use App\Image;
use App\Medical;
use App\Trainee;
use App\TraineePlans;
use App\User;
use Auth;
use DB;
use Illuminate\Http\Request;
use Validator;

class TraineeController extends Controller {

	function show($id) {
		$user = User::trainee()
			->with(['medicals' => function ($query) {
				$query->select('user_id', 'issue_type', 'issue_details', 'confirm');
			}])
			->select('id', 'first_name', 'last_name', 'gender', 'birth_date', 'goal', 'height', 'body_type', 'email', 'phone', 'address', 'city', 'profile_image_id')
			->whereId($id)
			->first();
		if ($user) {
			$user = $user->setAppends(['profile_image']);
			return $user->toArray();
		}
		return response()->json(['message' => "couldn't find trainee"], 404);
	}

	public function updateTrainee(Request $request, $id) {
		$validation = Validator::make($request->all(), [
			'email' => 'email|unique:users,id,' . $id,
			'phone' => 'string',
			'goal' => 'string',
			'type' => 'in:trainee',
			'birth_date' => 'date',
			'body_type' => 'in:mesomorph,endomorph,ectomorph',
			'height' => 'numeric',
			'profile_image' => 'base64_image',
			'medicals.*.user_id' => 'integer',
		]);
		if ($validation->fails()) {
			return response()->json($validation->messages(), 422);
		}
		DB::beginTransaction();
		try {
			$user = User::find($id);
			// update trainee
			if ($user) {
				$userData = $request->all();
				unset($userData['profile_image']);
				unset($userData['medicals']);
				$update = $user->update($userData);
				// profile image
				if ($request->has('profile_image')) {
					$filename = 'avatar-' . time() . '.jpg';
					$file = base64_to_jpeg($request['profile_image'], base_path('public/images') . '/' . $filename);
					// store in DB
					$profile = new Image();
					$profile->image_name = $filename;
					$profile->image_url = url('/images') . '/' . $filename;
					$user->images()->save($profile);
					$user->update(['profile_image_id' => $profile->id]);
				}
				// update medical histories
				if ($request['medicals']) {
					db::statement('set foreign_key_checks=0');
					$trainee = Medical::whereUserId($id)->delete();
					$insert = Medical::insert($request['medicals']);
					db::statement('set foreign_key_checks=1');
				}
				DB::commit();
				return $this->show($id);
			} else {
				DB::rollback();
				return response()->json(['message' => 'user not found 404!'], 404);
			}
		} catch (\Exception $e) {
			return response()->json(['message' => $e->getMessage()], 500);
		}
	}

	function traineeDashboard(Request $request, $trainee_id) {
		$trainer_id = Auth::user()->id;
		$trainee = User::trainee()
			->with([
				'dataPoints' => function ($query) {
					$query->select('user_id', 'point_type', 'point_value', 'created_at')
						->where('point_type', 'image')
						->orderBy('created_at', 'desc');
				},
				'medicals' => function ($query) {
					$query->select('user_id', 'issue_type', 'issue_details', 'confirm');
				},
			])
			->select('id', 'first_name', 'last_name', 'gender', 'birth_date', 'goal', 'height', 'body_type', 'email', 'phone', 'address', 'city', 'profile_image_id')
			->whereId($trainee_id)
			->first();
		if(!$trainee){
			return response()->json(['message' => 'trainee not found 404!'], 404);
		}
		// convert dataPoints relation to progressPhotos
		$progress_photos = $trainee->dataPoints->toArray();
		foreach ($progress_photos as $key => $photo) {
			$progress_photos[$key]['point_value'] = json_decode($photo['point_value'], true);
		}
		$trainee->progress_photos = $progress_photos;
		unset($trainee->dataPoints);
		// data points
		$dispatcher = app('Dingo\Api\Dispatcher');
		$trainee->graph = $dispatcher->get('data-points/' . $trainee_id);

		$traineePlans = [
			'nutritions' => [],
			'exercises' => [],
		];
		$plans = TraineePlans::where('trainee_id', $trainee_id)->get();
		foreach ($plans as $plan) {
			$model = $plan->planable_type;
			$item = $model::where('trainer_id', $trainer_id)
				->where('id', $plan->planable_id)
				->first();
			if ($item) {
				if ($plan->planable_type == 'App\\ExercisePlan') {
					$traineePlans['exercises'][] = $item;
				} else if ($plan->planable_type == 'App\\NutritionPlan') {
					$traineePlans['nutritions'] = $item;
				}
			}
		}
		$trainee->plans = $traineePlans;
		return $trainee->toArray();
	}
}
