<?php

namespace App\Http\Controllers;

use App\Payment;
use DB;
use App\User;
use App\Trainer;
use App\Training;
use App\Speciality;
use App\TrainerWorkOption;
use Illuminate\Http\Request;

class TrainerController extends Controller
{
    public function index(Request $request) // trainers list
    {
      $trainers = User::with(['specialities' => function ($query) use($request){
          $query->where('speciality', 'like', '%'.$request['speciality'].'%');
      }, 'trainerWorkOptions', 'images', 'availabilitySchedules'])
        ->whereType('trainer')
        ->where('first_name', 'like', '%'.$request['name'].'%')
        ->get();
        foreach ($trainers as $trainer){
            $trainer->specilaties_string=[];
            foreach ($trainer->specialities as $speciality){
                $trainer->specilaties_string=array_merge($trainer->specilaties_string,[$speciality->speciality]);
         }
            $trainer->specilaties_string = implode(',',$trainer->specilaties_string);
        }

        $list = [];
        if($request['rate']) // filter with rates
        {
            $rateVal = $request->rate + 0; // convert string to number
            foreach ($trainers as $trainer) {
                // if rate is integer, then get all trainers with this integer rate
                if(is_int($rateVal) && (floor($trainer->avgRating) == floor($rateVal))) array_push($list, $trainer);
                // if rate is a float number, then get trainers with that specific float rate
                else if(is_float($rateVal) && (round($trainer->avgRating, 1) == round($rateVal, 1))) array_push($list, $trainer);
            }
            return $list->toArray();
        }
        else return $trainers->toArray();
    }

    public function show($id) // trianer details
    {
        $trainer = User::trainer()
                        ->with([
                        'trainerWorkOptions',
                        'images' => function($query){
                            $query->select('imageable_id', 'imageable_type' ,'image_url');
                        },
                        'specialities' => function($query){
                            $query->select('id','speciality');
                        }])
                        ->where('id', $id)
                        ->select('first_name', 'last_name', 'about', 'id','email','birth_date','phone','city','lat','lng','gender','type')
                        ->first();
        if(!$trainer){
            return response()->json(['message' => 'trainer with id '. $id.' not found 404!'], 404);
        }

        # get booking status
        $trainer->request_status = null;
        $trainer->payment_status = null;
        $trainee_id = \Auth::user()->id; // the logged in user is supposed to be the trainee id
        $training =  Training::where('trainee_id', $trainee_id)
                                ->where('trainer_id', $id)
                                ->first();
        if($training){
            $trainer->request_status = $training->status;
            $trainer->payment_status = $training->paid == 0 ? 'pending' : 'approved';
        }

      return $trainer->toArray();
    }

    public function store(Request $request) // create new trainer work option
    {
        $this->validate($request, [
            'work_options.*.trainer_id' => 'required|integer',
            'work_options.*.speciality_id' => 'required|integer',
            'work_options.*.price' => 'required|numeric',
            'work_options.*.unit' => 'required|in:hour,session,month',
            'work_options.*.currency' => 'required',
            'work_options.*.work_option' => 'required|in:personal,group,online'
        ]);

        foreach ($request->get('work_options') as $req) {
            $trainer = User::find($req['trainer_id']);
            $speciality = Speciality::find($req['speciality_id']);

            if($trainer && $speciality){
                $duplicate = TrainerWorkOption::whereTrainerId($req['trainer_id'])
                    ->whereSpecialityId($req['speciality_id'])
                    ->whereWorkOption($req['work_option'])
                    ->whereUnit($req['unit'])
                    ->first();
                if (!$duplicate) {
                    $trainer_work = TrainerWorkOption::create($req);
                } else {
                    return [
                        'status_code' => 422,
                        'message' => 'duplicate assignment!, work option with trainer id '.$req['trainer_id'].' and specialty id '.$req['speciality_id'].' and work_option '.$req['work_option'].' and unit '.$req['unit'].' is already assigned'
                    ];
                }
            }
            else if($trainer == null) return ['status_code' => 404, 'message' => 'trainer with id '.$req['trainer_id'].' not found 404!'];
            else if($speciality == null) return ['status_code' => 404, 'message' => 'speciality with id '.$req['speciality_id'].' not found 404!'];
        }
        return ['status_code' => 200, 'message' => 'OK'];
    }

    public function destroy(Request $request) // destroy trainer work option
    {
        foreach ($request->all() as $value) {
            $work_option = TrainerWorkOption::find($value['id']);
            if($work_option == null) {
                return ['status_code' => 404, 'message' => 'work option with id '.$value['id'].' not found 404!'];
            }
        }
        TrainerWorkOption::destroy($request->all());
        return ['status_code' => 200, 'message' => 'OK'];
    }

    public function nearby(Request $request)
    {
      return Trainer::where('type', 'trainer')->where('city', 'like', '%'.$request->city.'%')->get()->toArray();
    }

    public function TrainerWorkOptions($trainer_id) // get all trainer work options
    {
        $trainer = User::find($trainer_id);
        if ($trainer) {
            return User::with('trainerWorkOptions')
                ->whereId($trainer_id)
                ->select('id', 'first_name', 'last_name', 'type', 'email', 'phone', 'city', 'lat', 'lng', 'height', 'birth_date', 'gender')
                ->first()->toArray();
            return ['status_code' => 404, 'message' => 'trainer not found 404!'];
        }
    }

    public function pendingRequests($id)
    {
        $trainer = Trainer::whereId($id)->whereType('trainer')->first();
        if ($trainer) {
            $requests = Training::where('trainer_id',$id)->where('status','requested')->orderBy('created_at','DESC')->get();
            foreach ($requests as $request){
                $request->trainee =User::where('id',$request->trainee_id)->with('images')->first();
            }
            return $requests->toArray();
        } else {
            return ['status_code' => 404, 'message' => 'trainer not found 404!'];
        }
    }

    public function earnings($id,Request $request)
    {
        $from = $request->input('from');
        $to = $request->input('to');
        $payments = Payment::where('trainer_id',$id)->whereBetween('created_at',array($from,$to))->orderBy('created_at','DESC')->get();
        if ($payments) {
            return $payments->toArray();
        } else {
            return ['status_code' => 404, 'message' => 'payment not found 404!'];
        }
    }



    public function trainerClients(Request $request, $trainer_id){
        $trainees = User::select('users.id', 'users.first_name', 'users.last_name', 'users.goal',
                                 'users.profile_image_id', 'trainings.created_at', 'trainings.status'
                            )
                       ->join('trainings', 'users.id', '=', 'trainings.trainee_id')
                       ->where('trainings.trainer_id', $trainer_id)
                       ->where('trainings.status', 'approved');
        if($request->has('name')){
            $trainees  = $trainees->where(\DB::raw("CONCAT(users.first_name, ' ', 'users.last_name')"), 'like' , '%'.$request['name'].'%');
        }
        $trainees = $trainees->orderBy('trainings.created_at', 'desc');
        $trainees = $trainees->paginate(10);
        foreach ($trainees as $trainee) {
            $trainee->setAppends(['profile_image']);
        }

        return $trainees;
    }
}
