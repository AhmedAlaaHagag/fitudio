<?php

namespace App\Http\Controllers;

use App\User;
use App\TrainerSchedule;
use Illuminate\Http\Request;

class TrainerScheduleController extends Controller
{
    public function index(Request $request)
    {
      return TrainerSchedule::with(['trainer' => function ($query){
          $query->with('images')->select('id', 'first_name', 'last_name', 'email', 'gender');
        }])
        ->where('day', 'like', '%'.$request['day'].'%')
        ->where('from', 'like', '%'.$request['from'].'%')
        ->where('to', 'like', '%'.$request['to'].'%')
        ->get()->toArray();
    }

    public function show($trainer_id)
    {
      return TrainerSchedule::with(['trainer' => function ($query){
        $query->with('events')->select('id');
    }])->whereTrainerId($trainer_id)->get()->toArray();
    }

    public function store(Request $request)
    {
      $this->validate($request, [
          'trainer_id' => 'required|integer',
          'day' => 'required',
          'from' => 'required',
          'to' => 'required',
          'location' => 'required'
      ]);

      if(User::find($request->trainer_id)){
        $input = $request->all();
        return TrainerSchedule::create($input)->toArray();
      }
      else return ['status_code' => 404, 'message' => 'trainer not found 404!'];
    }

    public function destroy($id)
    {
      $schedule = TrainerSchedule::find($id);
      if($schedule){
        $schedule->delete();
        return ['status_code' => 200];
      }
      else return ['status_code' => 404, 'message' => 'trainer schedule not found 404!'];
    }
}
