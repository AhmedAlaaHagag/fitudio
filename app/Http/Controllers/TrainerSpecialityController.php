<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Speciality;
use App\User;
use DB;

class TrainerSpecialityController extends Controller
{
    public function store(Request $request)
    {
        foreach ($request->all() as $req) {
            $duplicate = DB::table('trainers_specialities')->whereUserId($req['trainer_id'])->whereSpecialityId($req['speciality_id'])->first();
            if(!$duplicate){
                $user = User::find($req['trainer_id']);
                $speciality = Speciality::find($req['speciality_id']);
                if($user && $speciality){
                    $user->specialities()->attach($req['speciality_id']);
                }
                else if($user == null) return ['status_code' => 404, 'message' => 'trainer with id '.$req['trainer_id'].' not found 404!'];
                else if($speciality == null) return ['status_code' => 404, 'message' => 'speciality with id '.$req['speciality_id'].' not found 404!'];
            } else{
                return ['message' => 'speciality with id '.$req['speciality_id'].' already asigned to trainer with id '.$req['trainer_id'].' (duplicate assignment)!'];
            }
        }
        return ['status_code' => 200, 'message' => 'OK'];
    }

    public function destroy($user_id, $speciality_id)
    {
      $user = User::find($user_id);
      $speciality = Speciality::find($speciality_id);
      $user_speciality = DB::table('trainers_specialities')->where('user_id', $user_id)->where('speciality_id', $speciality_id)->first();
      if($user_speciality && $user && $speciality){
        $user->specialities()->detach($speciality_id);
        return ['status_code' => 200, 'message' => 'OK'];
        }
      else if($user == null) return ['status_code' => 404, 'message' => 'trainer not found 404!'];
      else if($speciality == null) return ['status_code' => 404, 'message' => 'speciality not found 404!'];
      else if($user_speciality == null) return ['status_code' => 404, 'message' => 'trainer speciality does not exist!'];
    }
}
