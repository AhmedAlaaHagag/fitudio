<?php

namespace App\Http\Controllers\Trainers;

use App\Http\Controllers\Controller;
use App\Payment;
use App\Rate;
use App\Trainer;
use App\Training;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    function __construct()
    {
    }

    public function index($id){ // Home Page
        $trainerId = $id;
        $homePage=[];
        $rate = sprintf ("%.1f", Rate::where('trainer_id',$trainerId)->avg('rate'));
        $earnings = Payment::where('trainer_id',$trainerId)->sum('amount');
        $requests = Training::where('trainer_id',$trainerId)->where('status','requested')->orderBy('created_at','DESC')->take(2)->get();
        foreach ($requests as $request){
            $request->trainee =User::where('id',$request->trainee_id)->with('images')->first();
        }
        $homePage['rate'] = (float)$rate;
        $homePage['earnings'] = (float)$earnings;
        $homePage['requests'] = $requests;
        return $homePage;
    }
}
