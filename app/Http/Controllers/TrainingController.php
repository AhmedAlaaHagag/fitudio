<?php

namespace App\Http\Controllers;

use App\TrainerWorkOption;
use App\Training;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TrainingController extends Controller
{
    public function index()
    {
        $trainings = Training::with('trainer', 'trainee')->get();
        return $trainings->toArray();
    }

    /**
     * Display trainer clients with all nutritions and exercises (filtered)
     *
     * @param \Illuminate\Http\Request $request
     * @param int $trainer_id
     * @return JSON
     */
    public function show(Request $request, $training_id)
    {
        $this->validate($request, [
            'start' => 'date'
        ]);

        $training = Training::with(['nutritionPlans' => function ($query) use ($request) {
            $query->where('day', 'like', '%' . $request['day'] . '%')
                ->get();
        }, 'exercisePlans' => function ($query) use ($request) {
            $query->where('day', 'like', '%' . $request['day'] . '%')
                ->get();
        }, 'trainer', 'trainee'])
            ->find($training_id);

        return $training->toArray();
    }

    /**
     * Display trainer clients with all nutritions and exercises (filtered)
     *
     * @param \Illuminate\Http\Request $request
     * @param int $trainer_id
     * @return JSON
     */
    public function trainerClients(Request $request, $trainer_id)
    {
        $this->validate($request, [
            'start' => 'date',
            'end' => 'date'
        ]);

        $clients = Training::with('workOptions', 'nutritionPlans', 'exercisePlans')
            ->whereTrainerId($trainer_id)
            ->join('users', 'users.id', 'trainings.trainee_id')
            ->select('users.id', 'users.first_name', 'users.last_name', 'users.email', 'users.phone', 'trainings.*')
            ->where('status', '!=', 'rejected')
            ->where('status', '!=', 'requested');
        if (isset($request['start']) && isset($request['end'])) {
            $clients->where('start_date', 'like', '%' . $request['start'] . '%')
                ->where('end_date', 'like', '%' . $request['end'] . '%');
        }
        $clients = $clients->get();

        return $clients->toArray();
    }

    public function request(Request $request)
    {
        $validation = $this->validate($request, [
            'trainer_id' => 'required|integer',
            'trainee_id' => 'required|integer',
        ]);
        $date = new Carbon();
        $trainer = User::find($request->trainer_id);
        $trainee = User::find($request->trainee_id);
        if ($trainer && $trainee) {
            $training = Training::create([
                'trainer_id' => $request->trainer_id,
                'trainee_id' => $request->trainee_id,
                'status' => 'requested',
                'start_date' => $date->toDateString(),
                'end_date' => $date->toDateString(),
                'address' => "",
                'request_comment' => null,
                'status' => 'approved'//TODO : Remove this
            ]);
            foreach ($request['work_options'] as $req) {
                $work_option = TrainerWorkOption::find($req['id']);
                if ($work_option) {
                    $training->workOptions()->attach($work_option, ['quantity' => 1]);

                } else {
                    return ['status_code' => 404, 'message' => 'work option with id ' . $req['id'] . ' not found 404!'];
                }
            }
            // send notification to trainer
            Training::notify($trainer->device_id_token, $trainee->first_name . ' has requested online training', $training->toArray(), 'training_request');
            return $training->toArray();
        } else if ($trainer == null) return ['status_code' => 404, 'message' => 'trainer not found 404!'];
        else if ($trainee == null) return ['status_code' => 404, 'message' => 'trainee not found 404!'];
    }

    public function response(Request $request)
    {
        $validation = $this->validate($request, [
            'training_id' => 'required|integer',
            'response' => 'required|in:approved,rejected,started,closed'
        ]);

        $input = $request->all();
        $training = Training::find($request->training_id);
        if ($training) {
            $training->update([
                'status' => $request->response,
                'response_comment' => $request->comment
            ]);
            // send notification to trainee
            $trainee = User::find($training->trainee_id);
            $trainer = User::find($training->trainer_id);
            //Training::notify($trainee->device_id_token, 'New training response', 'Coach '.$trainer->name.' has '.$request->response.' your training request.','training_request'); // (token, title, body)

            return $training->toArray();
        } else {
            return ['status_code' => 404, 'message' => 'training not found 404!'];
        }
    }

    public function destroy($id)
    {
        $training = Training::find($id);
        if ($training) {
            if (count($training->nutritionPlans) > 0) return ['message' => 'this training has nutrition plans!'];
            if (count($training->exercisePlans) > 0) return ['message' => 'this training has exercise plans!'];
            $training->delete();
            return ['status_code' => 200];
        } else {
            return ['status_code' => 404, 'message' => 'training not found 404!'];
        }
    }

    public function pending($id)
    {
        $payments = Training::with(['trainer', 'trainer.specialities', 'trainee'])
            ->whereTraineeId($id)
            ->wherePaid(0)
            ->wherestatus('approved')
            ->get();
        return $payments->toArray();
    }
}
