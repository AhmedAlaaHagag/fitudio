<?php

namespace App\Http\Controllers;

use App\TrainerSpecialities;
use App\TrainerWorkOption;
use DB;
use Auth;
use App\User;
use App\Image;
use App\OauthClient;
use App\PasswordReset;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request as Request;
use Illuminate\Support\Facades\Validator;

class UsersController extends Controller
{
    private $salt;
    protected $user;

    public function __construct()
    {
        $this->user = 'user';
        $this->salt="userloginregister";
    }

    protected function authenticateApp($request)
    {
        $client = OauthClient::wherePasswordClient(1)->first();

        if ($request->grant_type != 'password' || $request->client_id != $client->id || $request->client_secret != $client->secret) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $users = User::all();
        return $users;
    }
    /***/

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $user = User::with('images', 'dataPoints', 'medicals')->whereId($id)->first();
        if ($user) {
            return [
                'message' => 'trainee details',
                'data' => [
                    'token' => JWTAuth::fromUser($user)
                ],
                'user' => $user
            ];
        } else {
            return ['status_code' => 404, 'message'=>'user not found 404!'];
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'phone' => 'numeric',
            'password' => 'required|confirmed',
            'city' => 'required',
            'type' => 'required|in:admin,trainer,trainee',
            'lat' => 'required|numeric',
            'lng' => 'required|numeric',
            'birth_date' => 'required|date',
            'body_type' => 'in:mesomorph,endomorph,ectomorph',
            'height' => 'numeric',
            'grant_type' => 'required'
        ]);

        if ($validation->fails()){
            return response()->json($validation->messages(), 422);
        }

        if (User::whereEmail($request->email)->first()) {
            return ['status_code' => 412, 'message' => 'this email is already registered!'];
        }
        DB::beginTransaction();
        try{
            $activation_code = str_random(30);
            $input = $request->except(
                'profile_image_id',
                'activated',
                'activation_code',
                'created_at',
                'updated_at'
            );
            //$input['activation_code'] = $activation_code;
            $input['activation_code'] = '';
            $input['activated'] = 1;
            $input['password'] = app('hash')->make($request->password);
            $user = User::create($input);

            $token = JWTAuth::fromUser($user);
            JWTAuth::setToken($token);

            // profile image
            if ($request->has('profile_image')) {
                $filename = 'avatar-'.time().'.jpg';
                $file = base64_to_jpeg($request['profile_image'], base_path('/public/images').'/'.$filename);
                // store in DB
                $profile = new Image();
                $profile->image_name = $filename;
                $profile->image_url = url('/images').'/'.$filename;
                $user->images()->save($profile);
                $user->update(['profile_image_id' => $profile->id]);
            }
            // send verification mail
            $sent = Mail::send('email.verify', ['activation_code' => $activation_code], function ($message) use ($input) {
                $message->to($input['email'], $input['first_name'])->subject('Verify your account!.');
            });

            // Only Trainer
            if($request->input('type')=='trainer'){
              if($request->has('specialities')){
                $specialities = explode(',',$request->input('specialities'));
                  foreach ($specialities as $specialty){
                      $specialtiesObj = new TrainerSpecialities();
                      $specialtiesObj->user_id = $user->id;
                      $specialtiesObj->speciality_id= $specialty;
                      $specialtiesObj->save();
                  }
              }
              if($request->has('coaching_rate')){
                  $workOptions = new TrainerWorkOption();
                  $workOptions->trainer_id=$user->id;
                  $workOptions->type_id=1;
                  $workOptions->price=$request->input('coaching_rate');
                  $workOptions->work_option='online';
                  $workOptions->unit='month';
                  $workOptions->save();
              }
            }
            DB::commit();
            return [
                'message' => 'verified',
                'data' => [
                    'token' => $token
                ],
                'user' => $user
            ];
        }catch(\Exception $e){
            DB::rollback();
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $validation = $this->validate($request, [
          'email' => 'email',
          'phone' => 'numeric',
          'type' => 'in:admin,trainer,trainee',
          'lat' => 'numeric',
          'lng' => 'numeric',
          'birth_date' => 'date',
        ]);

        if (empty($validation)) {
            $user = User::find($id);
            if (User::whereEmail($request['email'])->where('email', '!=', $user['email'])->first()) {
                return ['status_code' => 412, 'message' => 'this email is already used!'];
            }
            if ($user) {
                $user->update($request->input());
                // profile image
                if ($request->has('profile_image')) {
                    $filename = 'avatar-'.time().'.jpg';
                    $file = base64_to_jpeg($request['profile_image'], base_path('/public/images').'/'.$filename);
                    // store in DB
                    $profile = new Image();
                    $profile->image_name = $filename;
                    $profile->image_url = url('/images').'/'.$filename;
                    $user->images()->save($profile);
                    $user->update(['profile_image_id' => $profile->id]);
                    $specialties = explode(',',$request->input('specialties'));
                    $deleted = TrainerSpecialities::where('user_id',$user->id)->delete();
                    foreach ($specialties as $specialty){
                        $specialtiesObj = new TrainerSpecialities();
                        $specialtiesObj->user_id = $user->id;
                        $specialtiesObj->speciality_id= $specialty;
                        $specialtiesObj->save();
                    }
                }
                return $user->toArray();
            } else {
                return ['status_code' => 404, 'message'=>'user not found 404!'];
            }
        } else {
            return $validation;
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if ($user) {
            $user->delete();
                return ['message'=>'user has been deleted'];
        } else {
            return ['status_code' => 404, 'message'=>'user not found 404!'];
        }
    }

    public function verify($token)
    {
        $user = User::whereActivationCode($token)->first();
        if ($user) {
            $user->update([
            'activated' => 1
            ]);
            view('email.activated');
            return ['status_code' => 200, 'message' => 'OK'];
        }
        return ['status_code' => 500, 'message' => 'Wrong Activation Code'];
    }


    public function deviceToken(Request $request)
    {
        $user = User::find($request->id);
        if ($user) {
            $user->update(['device_id_token' => $request->device_token]);
            return $user->toArray();
        } else {
            return ['status_code' => 404, 'message' => 'user not found 404!'];
        }
    }

    public function reactivate(Request $request)
    {
        $this->validate($request, [
            'grant_type' => 'required',
            'email' => 'required|email',
            'client_secret' => 'required'
        ]);

        if (!$this->authenticateApp($request)) {
            return ['status_code' => 401, 'message' => 'unauthenticated app'];
        }

        $user = User::where('email',$request->input('email'))->first();
        if ($user) {
            $activation_code = str_random(30);
            $user->update(['activation_code' => $activation_code]);
            // send verification mail
            Mail::send('email.verify', ['activation_code' => $activation_code], function ($message) use ($user) {
                $message->to($user->email, $user->name)->subject('Verify your account!.');
            });

            return ['status_code' => 200, 'message' => 'OK'];
        } else {
            return ['status_code' => 404, 'message' => 'user not found 404!'];
        }
    }

    public function reset(Request $request, $id)
    {
        $this->validate($request, [
            'grant_type' => 'required',
            'email' => 'required|email'
        ]);

        $user = User::find($id);
        if ($user) {
            $password = str_random(10);
            $hashed = app('hash')->make($password);
            $user->update(['password' => $hashed]);
            // send verification mail
            Mail::send('email.reset', ['password' => $password], function ($message) use ($user) {
                $message->to($user->email, $user->name)->subject('Password Reset.');
            });
            return ['status_code' => 200, 'message' => 'OK'];
        } else {
            return ['status_code' => 404, 'message' => 'user not found 404!'];
        }
    }

    public function forgotPassword(Request $request){
        $this->validate($request, [
            'email' => 'required|email'
        ]);
        $user = User::whereEmail($request->email)->first();
        if(!$user){
            return response()->json(['message'=>'user not found 404!'], 404);
        }
        $token =  mt_rand(100000, 999999);
        $passwordReset = new PasswordReset([
            'email' => $request->email,
            'token' => $token
        ]);
        $passwordReset->timestamps = false;
        $passwordReset->save();
        Mail::send('email.reset', ['token' => $token], function ($message) use ($user,$request) {
            $message->to($request->email, $user->name)->subject('Password Reset.');
        });
        return ['status_code' => 200, 'message' => 'OK'];
    }


    function updatePassword(Request $request){
       /* $this->validate($request, [
            'code' => 'required',
            'password' => 'password',
            'confirm_password' => 'same:password',
            'email' => 'required|email'
        ]);*/
        $email = $request->input('email');
        $token = $request->input('code');
        $user = User::whereEmail($email)->first();
        if(!$user){
            return response()->json(['message'=>'user not found 404!'], 404);
        }

        $token = PasswordReset::whereEmail($email)->whereToken($token)->first();
        if(!$token){
            return response()->json(['message'=>'You dont have any password reset request!'], 403);
        }

        $user->password =  app('hash')->make($request->password);
        $user->save();
        $q = 'DELETE FROM password_resets where email = ? AND token = ?';
        \DB::delete($q, [$email,$token->token]);
        return ['status_code' => 200, 'message' => 'OK'];
    }

    public function updateToken(Request $request)
    {
        $this->validate($request,[
            'user_id'=>'required|integer|exists:users,id',
            'device_type'=>'in:android,ios',
            'device_token'=>'required'
        ]);
        $user = User::find($request->user_id);
        $user->device_id_token = $request->device_token ;
        $save = $user->save();
        return $save ? ['status_code' => 200, 'message' => 'OK'] : ['status_code' => 401, 'message' => 'can\'t update token '];
    }
}
