<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table = 'images';
    
    protected $fillable = ['imageable_id', 'imageable_type', 'images_url'];

    public function imageable()
    {
      return $this->morphTo();
    }
}
