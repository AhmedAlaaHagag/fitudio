<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Medical extends Model
{
    protected $table = 'medical_histories';
    protected $fillable = [
      'user_id',
      'issue_type',
      'issue_details',
      'confirm'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
