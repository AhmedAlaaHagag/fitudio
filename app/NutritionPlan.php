<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NutritionPlan extends Model {
	protected $table = 'nutritions_plans';
	protected $guarded = [];
}
