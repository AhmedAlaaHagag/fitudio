<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NutritionPlanDays extends Model
{
    //
    public $timestamps = false;
    protected $fillable = ['nutrition_plan_id', 'day'];
    protected $appends = ['meals'];
    protected $hidden = ['id'];
    public function getMealsAttribute()
    {
        return $this->meals();
    }

    public function meals()
    {
        return $this->hasMany(NutritionPlanMeals::class,'day_id')->get();
    }
}
