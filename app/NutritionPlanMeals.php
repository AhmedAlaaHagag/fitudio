<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NutritionPlanMeals extends Model
{
    public $timestamps = false;
    protected $fillable = ['type','number_of_serving','size','NDB_No','supplement_id','day_id','name'];
}
