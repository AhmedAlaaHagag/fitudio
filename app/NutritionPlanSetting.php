<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NutritionPlanSetting extends Model
{
    protected $table = 'nutrition_plan_settings';
    protected $fillable = ['nutrition_id','plan_start','plan_end','week_start'];
    public $timestamps = false;
}
