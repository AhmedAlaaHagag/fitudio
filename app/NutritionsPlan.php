<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NutritionsPlan extends Model
{
    //
    protected $fillable = ['recommendation','name','trainer_id'];

    protected $appends = ['days'];


    public function getDaysAttribute(){
        return $this->days();
    }

    public function days()
    {
        return $this->hasMany(NutritionPlanDays::class,'nutrition_plan_id')->get();
    }
}
