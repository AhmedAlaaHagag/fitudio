<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = 'payments';

    protected $fillable = [
        'trainer_id',
        'amount',
        'email',
        'charge',
        'payable_id',
        'payable_type'
    ];

    public function payable()
    {
      return $this->morphTo();
    }
}
