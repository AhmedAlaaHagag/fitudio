<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{



    function boot(){
      \Validator::extend('base64_image', function ($attribute, $value, $parameters, $validator) {
          $value = preg_replace("/data:image\/(.*);base64,/", '', $value);
          return base64_decode($value, true);
      });
    }


    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
      $this->app->singleton('mailer', function ($app) {
          $app->configure('services');
          return $app->loadComponent('mail', 'Illuminate\Mail\MailServiceProvider', 'mailer');
      });
    }
}
