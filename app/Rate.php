<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    protected $table = 'rates';
    protected $fillable = ['trainer_id','trainee_id', 'rate', 'comment'];



    /**
     * register model events
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        // send welcome email after creating new records
        static::created(function($rate) {
            // send a notification to trainer
        });
    }

    public function trainer()
    {
      return $this->belongsTo('App\User', 'trainer_id');
    }
}
