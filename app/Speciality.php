<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Speciality extends Model
{
    protected $table = 'specialities';
    protected $fillable = ['speciality'];
    protected $hidden = array('pivot', 'created_at', 'updated_at');

    public function trainers()
    {
      return $this->belongsToMany('App\User', 'trainers_specialities', 'speciality_id', 'user_id');
    }

    public function trainersTypes()
    {
      return $this->hasMany('App\TrainerType', 'type_id', 'id');
    }
}
