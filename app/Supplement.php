<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplement extends Model
{
    protected $table = 'supplements';
    protected $fillable = ['name','description', 'supplement_type_id'];
    protected $appends = ['category'];
    protected $hidden = ['supplement_type_id'];



    public function category(){
    	return $this->belongsTo(\App\SupplementType::class, 'supplement_type_id' ,'id');
    }


    public function getCategoryAttribute(){
    	return $this->category()->first()->name;
    }

}
