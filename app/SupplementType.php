<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupplementType extends Model
{
    protected $table = 'supplements_types';
    protected $fillable = ['name'];
}
