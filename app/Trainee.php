<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trainee extends Model
{
  protected $table = 'users';
  protected $fillable = [
      'name',
      'email',
      'phone',
      'password',
      'type',
      'gender',
      'address',
      'city',
      'lat',
      'lng',
      'social_provider_id',
      'social_provider_type',
      'birth_date',
      'addtional_data',
      'about',
      'payment_gateway_customer_id',
      'profile_image_id',
      'body_type',
      'height',
      'device_id_token',
      'stripe_id',
      'card_brand',
      'card_last_four',
      'trial_ends_at',
  ];
  protected $hidden = [
      'password',
      'remember_token',
      'social_provider_token'
  ];

  public function followings()
  {
    return $this->hasMany('App\Follow', 'trainee_id', 'id');
  }
}
