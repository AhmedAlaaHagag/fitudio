<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TraineePlans extends Model {
	public $timestamps = false;
	protected $fillable = ['trainee_id', 'planable_id', 'planable_type'];
	protected $table = 'trainee_plans';

	public function planable() {
		return $this->morphTo();
	}

	public function exercises() {
		return $this->morphedByMany('App\ExercisePlan', 'planable');
	}

	public function nutrations() {
		return $this->morphedByMany('App\NutritionPlan', 'planable');
	}
}
