<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trainer extends Model
{
    protected $table = 'users';
    protected $fillable = [
        'name',
        'email',
        'phone',
        'password',
        'type',
        'gender',
        'address',
        'city',
        'lat',
        'lng',
        'social_provider_id',
        'social_provider_type',
        'birth_date',
        'addtional_data',
        'about',
        'payment_gateway_customer_id',
        'profile_image_id',
        'body_type',
        'height',
        'device_id_token',
        'stripe_id',
        'card_brand',
        'card_last_four',
        'trial_ends_at',
    ];
    protected $hidden = [
        'password',
        'remember_token',
        'social_provider_token'
    ];

    public function followers()
    {
      return $this->hasMany('App\Follow', 'trainer_id', 'id');
    }

    public function trainerWorkOptions()
    {
      return $this->hasMany('App\TrainerWorkOption', 'trainer_id', 'id');
    }



    public function trainees(){
        return $this->belongsToMany(\App\Trainee::class, 'trainings', 'trainer_id', 'trainee_id')
                    ->withPivot('status');
    }
}
