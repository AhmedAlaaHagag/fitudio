<?php

namespace App;

use App\Event;
use Illuminate\Database\Eloquent\Model;

class TrainerSchedule extends Model
{
    protected $table = 'trainers_schedule';
    protected $fillable = [
      'trainer_id',
      'day',
      'from',
      'to',
      'location'
    ];

    public function trainer()
    {
      return $this->belongsTo('App\User', 'trainer_id');
    }
}
