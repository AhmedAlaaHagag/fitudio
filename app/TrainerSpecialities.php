<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrainerSpecialities extends Model
{
    protected $table = 'trainers_specialities';
}
