<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrainerWorkOption extends Model
{
  protected $table = 'trainers_work_options';
  protected $fillable = ['trainer_id', 'speciality_id', 'price', 'unit', 'currency', 'work_option'];

  public function trainers()
  {
    return $this->blongsTo('App\User', 'trainer_id');
  }

  public function types()
  {
    return $this->belongTo('App\Speciality', 'type_id');
  }
}
