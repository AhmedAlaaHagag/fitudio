<?php

namespace App;

use DB;
use App\Setting;
use App\Traits\Notification;
use Illuminate\Database\Eloquent\Model;

class Training extends Model
{
    use Notification;
    protected $table = 'trainings';

    protected $fillable = [
            'trainer_id',
            'work_option_id',
            'trainee_id',
            'type',
            'status',
            'start_date',
            'end_date',
            'address',
            'request_comment',
            'response_comment',
            'paid'
          ];

    protected $appends = ['totalFees'];

    public function nutritionPlans()
    {
        return $this->hasMany('App\TrainingNutrition', 'training_id', 'id');
    }

    public function exercisePlans()
    {
        return $this->hasMany('App\ExercisePlan', 'training_id', 'id');
    }

    public function trainer()
    {
        return $this->belongsTo('App\User')->select('id', 'first_name', 'last_name', 'email', 'phone');
    }

    public function workOptions()
    {
        return $this->belongsToMany('App\TrainerWorkOption', 'trainings_work_options', 'training_id', 'work_option_id')
            ->withPivot('quantity');
    }

    public function payments()
    {
      return $this->morphMany('App\Payment', 'payable');
    }

    public function trainee()
    {
        return $this->belongsTo('App\Trainee','trainer_id','id')->select('id', 'first_name', 'last_name', 'email', 'phone','goal');
    }

    public function gettotalFeesAttribute()
    {
        $comission = Setting::whereKey('comission')->first();
        if($comission){
            return $this->workOptions()
                ->select(DB::raw('round(quantity * price + ('.$comission->value.' / price) * 100, 2) as total_fees'))
                ->get()
                ->pluck('total_fees')->sum();
        }

    }
}
