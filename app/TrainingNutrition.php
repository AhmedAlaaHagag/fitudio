<?php

namespace App;

use App\Traits\Notification;
use Illuminate\Database\Eloquent\Model;

class TrainingNutrition extends Model
{
    use Notification;
    protected $table = 'training_nutrition';
    protected $fillable = [
      'training_id',
      'nutrition_fact_NDB_no',
      'day',
      'nutrition_type',
      'nutrition_description',
      'comment',
      'ammount',
      'unit'
    ];


    public function training()
    {
        return $this->belongsTo(Training::class);
    }
}
