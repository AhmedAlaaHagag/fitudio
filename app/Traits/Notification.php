<?php

namespace App\Traits;

use App\Notification as Notificate;
use App\User;
use Symfony\Component\HttpKernel\Exception\HttpException;

trait Notification
{
    /**
     * Send notification via FireBase, and store notification in DB
     *
     * @param string $token , string $title, array $body , string $type
     *
     * @return void
     */
    static public function notify($token, $title, $body, $type)
    {
        // Store notificatios in DB
        try {
            $database_notification = self::createNotification($token, $title, $body, $type);
            self::SendNotification($token, $title, $body, $type);
        } catch (\Exception $exception) {
            //Log $exception
        }

        return $database_notification;

    }

    /**
     * @param $token
     * @param $title
     * @param $body
     * @param $type
     */
    private static function createNotification($token, $title, $body, $type)
    {
        $user = User::where('device_id_token', $token)->first();
        if ($user) {
            Notificate::create([
                'user_id' => $user->id,
                'title' => $title,
                'body' => $body,
                'type' => $type,
            ]);
        }
    }

    /**
     * @param $token
     * @param $title
     * @param $body
     * @param $type
     */
    private static function SendNotification($token, $title, $body, $type)
    {
        try{
            if ($token) {
                $ch = curl_init("https://fcm.googleapis.com/fcm/send");
                // Creating the notification array.
                $notification = [
                    'type' => $type,
                    'title' => $title,
                    'message' => $body
                ];
                // This array contains, the token and the notification. The 'to' attribute stores the token.
                $arrayToSend = ['to' => $token, 'data' => $notification];
                //Generating JSON encoded string form the above array.
                $json = json_encode($arrayToSend);
                // Setup headers:
                $headers = array();
                $headers[] = 'Content-Type: application/json';
                $headers[] = 'Authorization: key=' . env('FIREBASE_API_KEY');
                // Setup curl, add headers and post parameters.
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
                curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                // Send the request
                curl_exec($ch);
                // Close request
                curl_close($ch);
            }
        }catch (\Exception $exception){

        }

    }
}

?>
