<?php

namespace App;

use Laravel\Cashier\Billable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class User extends Model implements
    AuthenticatableContract,
    AuthorizableContract
{
    use Authenticatable, Authorizable, Billable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'phone',
        'password',
        'type',
        'gender',
        'address',
        'city',
        'lat',
        'lng',
        'social_provider_id',
        'social_provider_type',
        'social_provider_token',
        'birth_date',
        'addtional_data',
        'about',
        'payment_gateway_customer_id',
        'profile_image_id',
        'body_type',
        'height',
        'activation_code',
        'activated',
        'device_id_token',
        'stripe_id',
        'card_brand',
        'card_last_four',
        'trial_ends_at',
        'goal'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'social_provider_token'
    ];

    protected $appends = ['avgRate', 'profile_image'];


    /**
     * register model events
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        // send welcome email after creating new records
        static::created(function($user) {
            $template = $user->type == 'trainee' ? 'email.trainee.welcome' : 'email.trainer.welcome';
            $sent = Mail::send($template, [], function ($message) use ($user) {
                $message->to($user['email'], $user['first_name'])->subject('Welcome To Fitudio!.');
            });
        });
    }   



    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function profileImage()
    {
        return $this->hasOne('App\Image', 'id', 'profile_image_id')
            ->where('imageable_type', 'App\\User');
    }

    public function images()
    {
        return $this->morphMany('App\Image', 'imageable');
    }

    public function specialities()
    {
        return $this->belongsToMany('App\Speciality', 'trainers_specialities', 'user_id', 'speciality_id');
    }

    public function trainerWorkOptions()
    {
        return $this->hasMany('App\TrainerWorkOption', 'trainer_id', 'id');
    }

    public function events()
    {
        return $this->belongsToMany('App\Event', 'users_events', 'user_id', 'event_id')->withTimestamps();
    }

    public function rates()
    {
        return $this->hasMany('App\Rate', 'trainer_id', 'id');
    }

    public function getAvgRateAttribute()
    {
        return $this->rates()->avg('rate');
    }

    public function getmaxRateAttribute()
    {
        return $this->rates()->max('rate');
    }


    public function getProfileImageAttribute()
    {
        $user = $this->profileImage()->first();
        return $user ? $this->profileImage()->first()->image_url : null;
    }

    public function getminRateAttribute()
    {
        return $this->rates()->min('rate');
    }

    public function dataPoints()
    {
        return $this->hasMany('App\DataPoint');
    }

    public function medicals()
    {
        return $this->hasMany('App\Medical');
    }

    public function availabilitySchedules()
    {
        return $this->hasMany('App\TrainerSchedule', 'trainer_id', 'id');
    }

    public function scopeTrainee($query)
    {
        return $query->whereType('trainee');
    }

    public function scopeTrainer($query)
    {
        return $query->whereType('trainer');
    }

    public function savedPlan()
    {
        if($this->type == 'trainer'){
            return $this->hasMany(ExercisePlan::class);
        }
    }
}
