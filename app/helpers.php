<?php

/**
 * Because Lumen has no config_path function, we need to add this function
 * to make JWT Auth works.
 */
if (!function_exists('config_path')) {
    /**
     * Get the configuration path.
     *
     * @param string $path
     *
     * @return string
     */
    function config_path($path = '')
    {
        return app()->basePath().'/config'.($path ? '/'.$path : $path);
    }
}

/**
 * Because Lumen has no storage_path function, we need to add this function
 * to make caching configuration works.
 */
if (!function_exists('storage_path')) {
    /**
     * Get the configuration path.
     *
     * @param string $path
     *
     * @return string
     */
    function storage_path($path = '')
    {
        return app()->basePath().'/storage'.($path ? '/'.$path : $path);
    }
}

/**
 * Convert base64 string to image
 */
function base64_to_jpeg($base64_string, $output_file) {
    // open the output file for writing
    $file = fopen( $output_file, 'wb' );

    // split the string on commas
    // $data[0] == "data:image/png;base64"
    // $data[1] == <actual base64 string>
    $data = explode( ',', $base64_string );

    // wirte down image
    fwrite( $file, base64_decode( $data[1] ) );

    // clean up the file resource
    fclose( $file );

    return $output_file;
}

/**
 * Get enum values from field in DB table
 */
 function getEnumValues($table, $field)
 {
     $type = DB::select( DB::raw("SHOW COLUMNS FROM {$table} WHERE Field = '{$field}'") );
     preg_match("/^enum\(\'(.*)\'\)$/", $type[0]->Type, $matches);
     $enum = explode("','", $matches[1]);
     return $enum;
 }
