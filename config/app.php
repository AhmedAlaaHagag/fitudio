<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Encryption Key
    |--------------------------------------------------------------------------
    |
    | This key is used by the Illuminate encrypter service and should be set
    | to a random, 32 character string, otherwise these encrypted strings
    | will not be safe. Please do this before deploying an application!
    |
    */
   
    'locale' => env('APP_LOCALE', 'en'),
    
    'key' => env('APP_KEY'),

    'cipher' => 'AES-256-CBC',

    'pagination_value'=>'10',

    'providers' => [
      Laravel\Cashier\CashierServiceProvider::class,
    ],
];
