<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function ($faker) {
    return [
        'first_name' => $faker->name,
        'last_name' => $faker->name,
        'email' => $faker->email,
    ];
});
$factory->define(\App\Notification::class, function ($faker) {
   return [
       'user_id'=>\App\User::find(2)->id,
       'type'=>['training_request', 'payment', 'rating', 'reminder', 'exercise_plan','nutrition_plan', 'follow',][rand(1,6)],
       'title'=>$faker->text,
       'body'=>json_encode(['training_request', 'payment', 'rating', 'reminder', 'exercise_plan','nutrition_plan', 'follow',])
   ];
});