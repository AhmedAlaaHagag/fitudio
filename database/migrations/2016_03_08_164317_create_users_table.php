<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
      	    $table->enum('type', ['trainer', 'trainee', 'admin']);
       	    $table->string('address');
      	    $table->enum('gender', ['male', 'female'])->nullable();
            $table->string('email')->unique();
            $table->string('phone', 20);
            $table->string('password', 60);
            $table->rememberToken();
            $table->string('social_provider_id', 45);
            $table->string('social_provider_type', 45);
            $table->string('social_provider_token', 45);
            $table->date('birth_date');
            $table->mediumText('addtional_data');
            $table->text('about');
            $table->string('payment_gateway_customer_id', 45);
            $table->integer('profile_image_id');
            $table->enum('body_type', ['mesomorph', 'endomorph', 'ectomorph']);
            $table->float('height')->comment('in cm');
            $table->boolean('activated')->default(false);
            $table->string('activation_code')->nullable();
            $table->string('device_id_token', 300);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
