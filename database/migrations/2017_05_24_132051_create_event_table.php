<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('image_id')->nullable();
            $table->string('title', 45);
            $table->string('description', 450);
            $table->dateTime('from');
            $table->dateTime('to');
            $table->decimal('participant_fees', 10, 2);
            $table->decimal('subscriber_fees', 10, 2);
            $table->string('fees_currency', 45);
            $table->string('address', 45);
            $table->string('lat');
            $table->string('lng');
            $table->string('type', 45)->comment('gym/group');
            $table->string('created_by');
            $table->boolean('is_featured');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
