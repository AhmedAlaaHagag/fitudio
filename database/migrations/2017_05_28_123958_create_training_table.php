<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trainings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('trainer_id')->unsigned();
            $table->integer('trainee_id')->unsigned();
            $table->enum('type', ['online', 'personal', 'group'])->comment('online/personal/group');
            $table->enum('status', ['requested', 'approved', 'rejected', 'started', 'closed'])->comment('requested/approved/rejected/started/closed');
            $table->dateTime('start_date');
            $table->dateTime('end_date');
            $table->string('address');
            $table->text('request_comment');
            $table->text('response_comment');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trainings');
    }
}
