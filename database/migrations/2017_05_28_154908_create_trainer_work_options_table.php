<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainerWorkOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trainers_work_options', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('trainer_id')->unsigned();
            $table->integer('speciality_id')->unsigned();
            $table->float('price');
            $table->string('currency', 45);
            $table->enum('work_option', ['personal', 'group', 'online'])->comment('personal, group, online');
            $table->enum('unit', ['hour', 'session', 'month'])->comment('hour, session, or month');
            $table->foreign('trainer_id')->references('id')->on('users');
            $table->foreign('speciality_id')->references('id')->on('specialities');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trainers_work_options');
    }
}
