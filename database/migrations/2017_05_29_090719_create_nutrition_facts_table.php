<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNutritionFactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nutrition_facts', function (Blueprint $table) {
            $table->string('NDB_No');
            $table->string('Shrt_Desc');
            $table->string('Water_(g)');
            $table->string('Energ_Kcal');
            $table->string('Protein_(g)');
            $table->string('Lipid_Tot_(g)');
            $table->string('Ash_(g)');
            $table->string('Carbohydrt_(g)');
            $table->string('Fiber_TD_(g)');
            $table->string('Sugar_Tot_(g)');
            $table->string('Calcium_(mg)');
            $table->string('Iron_(mg)');
            $table->string('Magnesium_(mg)');
            $table->string('Phosphorus_(mg)');
            $table->string('Potassium_(mg)');
            $table->string('Sodium_(mg)');
            $table->string('Zinc_(mg)');
            $table->string('Copper_mg)');
            $table->string('Manganese_(mg)');
            $table->string('Selenium_(µg)');
            $table->string('Vit_C_(mg)');
            $table->string('Thiamin_(mg)');
            $table->string('Riboflavin_(mg)');
            $table->string('Niacin_(mg)');
            $table->string('Panto_Acid_mg)');
            $table->string('Vit_B6_(mg)');
            $table->string('Folate_Tot_(µg)');
            $table->string('Folic_Acid_(µg)');
            $table->string('Food_Folate_(µg)');
            $table->string('Folate_DFE_(µg)');
            $table->string('Choline_Tot_ (mg)');
            $table->string('Vit_B12_(µg)');
            $table->string('Vit_A_IU');
            $table->string('Vit_A_RAE');
            $table->string('Retinol_(µg)');
            $table->string('Alpha_Carot_(µg)');
            $table->string('Beta_Carot_(µg)');
            $table->string('Beta_Crypt_(µg)');
            $table->string('Lycopene_(µg)');
            $table->string('Lut+Zea_ (µg)');
            $table->string('Vit_E_(mg)');
            $table->string('Vit_D_µg');
            $table->string('Vit_D_IU');
            $table->string('Vit_K_(µg)');
            $table->string('FA_Sat_(g)');
            $table->string('FA_Mono_(g)');
            $table->string('FA_Poly_(g)');
            $table->string('Cholestrl_(mg)');
            $table->string('GmWt_1');
            $table->string('GmWt_Desc1');
            $table->string('GmWt_2');
            $table->string('GmWt_Desc2');
            $table->string('Refuse_Pct');
            $table->primary('NDB_No');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nutrition_facts');
    }
}
