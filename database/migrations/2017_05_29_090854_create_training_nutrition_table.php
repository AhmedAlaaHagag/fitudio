<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainingNutritionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('training_nutrition', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('training_id')->unsigned();
            $table->string('nutrition_fact_NDB_no');
            $table->date('day');
            $table->string('nutrition_type', 45)->comment('breakfast, lunch, dinner, snack');
            $table->string('nutrition_description');
            $table->text('comment');
            $table->float('ammount');
            $table->string('unit', 45);
            $table->foreign('training_id')->references('id')->on('trainings');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('training_nutrition');
    }
}
