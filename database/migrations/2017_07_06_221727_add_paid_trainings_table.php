<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPaidTrainingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trainings', function (Blueprint $table) {
            $table->dropColumn('request_comment');
            $table->dropColumn('response_comment');
        });

        Schema::table('trainings', function (Blueprint $table) {
            $table->text('request_comment')->nullable()->after('address');
            $table->text('response_comment')->nullable()->after('request_comment');
            $table->boolean('paid')->default(0)->after('response_comment');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trainings', function (Blueprint $table) {
            $table->dropColumn('paid');
        });
    }
}
