<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnUserDataPointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_data_points', function (Blueprint $table) {
            $table->dropColumn('data_point_type');
            $table->dropColumn('data_point_unit');
            $table->dropColumn('data_point_value');
            $table->dropColumn('data_point_date');

            $table->float('weight')->nullable()->after('user_id');
            $table->float('fats')->nullable()->after('weight');
            $table->float('water_mass')->nullable()->after('fats');
            $table->float('muscles_mass')->nullable()->after('water_mass');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_data_points', function (Blueprint $table) {
            $table->dropColumn('weight');
            $table->dropColumn('fats');
            $table->dropColumn('water_mass');
            $table->dropColumn('muscles_mass');

            $table->string('data_point_type')->after('user_id');
            $table->string('data_point_unit')->after('data_point_type');
            $table->string('data_point_value')->after('data_point_unit');
            $table->date('data_point_date')->after('data_point_value');
        });
    }
}
