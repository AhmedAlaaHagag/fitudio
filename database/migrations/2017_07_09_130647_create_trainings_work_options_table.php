<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainingsWorkOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trainings', function (Blueprint $table) {
            $table->dropForeign('trainings_work_option_id_foreign');
            $table->dropColumn('work_option_id');
            $table->dropColumn('type');
        });

        Schema::create('trainings_work_options', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('training_id')->unsigned();
            $table->integer('work_option_id')->unsigned();
            $table->foreign('training_id')->references('id')->on('trainings')->onDelete('cascade');
            $table->foreign('work_option_id')->references('id')->on('trainers_work_options')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trainings', function (Blueprint $table) {
            $table->integer('work_option_id')->after('trainer_id')->unsigned();
            $table->enum('type', ['online', 'personal', 'group'])->after('status');
            $table->foreign('work_option_id')->references('id')->on('trainers_work_options')->onDelete('cascade');
        });

        Schema::dropIfExists('trainings_work_options');
    }
}
