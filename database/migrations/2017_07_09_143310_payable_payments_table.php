<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PayablePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->integer('payable_id')->after('charge');
            $table->string('payable_type')->after('payable_id');
            $table->dropForeign('payments_trainer_id_foreign');
            $table->dropColumn('trainer_id');
        });

        Schema::table('payments', function (Blueprint $table) {
            $table->integer('trainer_id')->after('id')->nullable()->unsigned();
            $table->foreign('trainer_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->dropColumn('payable_id');
            $table->dropColumn('payable_type');
            $table->dropForeign('payments_trainer_id_foreign');
            $table->dropColumn('trainer_id');
        });

        Schema::table('payments', function (Blueprint $table) {
            $table->integer('trainer_id')->after('id')->unsigned();
            $table->foreign('trainer_id')->references('id')->on('users')->onDelete('cascade');
        });
    }
}
