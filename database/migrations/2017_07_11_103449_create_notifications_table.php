<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $notification_types = ['training_request', 'payment', 'rating', 'reminder', 'exercise_plan','nutrition_plan', 'follow'];
        Schema::create('notifications', function (Blueprint $table) use ($notification_types) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->enum('type', $notification_types)->nullable();
            $table->string('title');
            $table->json('body');
            $table->boolean('read')->default(0);
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
