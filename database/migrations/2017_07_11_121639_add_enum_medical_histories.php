<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEnumMedicalHistories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('medical_histories', function (Blueprint $table) {
            $table->dropColumn('issue_type');
        });

        Schema::table('medical_histories', function (Blueprint $table) {
            $table->enum('issue_type', [
                'Diabteic',
                'Injury',
                'Heart Condition',
                'Previous Surgeries',
                'Allergies',
                'Bone or joint problem',
                'Blood pressure issues',
                'Cancer',
                'Neck back or knees problems',
                'Medication',
                'Pain or Dizziness'
            ])->after('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('medical_histories', function (Blueprint $table) {
            $table->dropColumn('issue_type');
        });

        Schema::table('medical_histories', function (Blueprint $table) {
            $table->string('issue_type')->after('user_id');
        });
    }
}
