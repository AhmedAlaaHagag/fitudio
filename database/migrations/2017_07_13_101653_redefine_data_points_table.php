<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RedefineDataPointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_data_points', function (Blueprint $table) {
            $table->dropColumn('weight');
            $table->dropColumn('fats');
            $table->dropColumn('water_mass');
            $table->dropColumn('muscles_mass');
        });

        Schema::table('user_data_points', function (Blueprint $table) {
            $table->string('point_type')->after('user_id');
            $table->float('point_value')->after('point_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_data_points', function (Blueprint $table) {
            $table->dropColumn('point_type');
            $table->dropColumn('point_value');
        });

        Schema::table('user_data_points', function (Blueprint $table) {
            $table->float('weight')->after('user_id');
            $table->float('fats')->after('weight');
            $table->float('water_mass')->after('fats');
            $table->float('muscles_mass')->after('water_mass');
        });
    }
}
