<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColsToExercisesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Schema::dropIfExists('exercises');
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        Schema::create('exercises', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->enum('target_area', ['Lower Body', 'Upper Body']);
            $table->integer('muscle_id')->unsigned();
            $table->foreign('muscle_id')->references('id')->on('muscles')->onDelete('cascade');
            $table->integer('exercise_type_id')->unsigned();
            $table->foreign('exercise_type_id')->references('id')->on('exercises_types')->onDelete('cascade');
            $table->string('equipment_needed')->nullable();
            $table->enum('level', ['Intermediate', 'Expert', 'Beginner'])->nullable();
            $table->string('alternative_name')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Schema::dropIfExists('exercises');
        Schema::create('exercises', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');

    }
}
