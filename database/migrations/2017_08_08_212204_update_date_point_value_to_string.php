<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDatePointValueToString extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_data_points', function (Blueprint $table) {
            \DB::statement('ALTER TABLE user_data_points MODIFY point_value varchar(255);');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_data_points', function (Blueprint $table) {
            \DB::statement('ALTER TABLE user_data_points MODIFY point_value decimal(5,2);');
        });
    }
}
