<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExercisePlanSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exercises_plan_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('exercises_id');
            $table->date('plan_start');
            $table->date('plan_end');
            $table->enum('week_start', config('dateTimeHelper.week_days'));

        });

        Schema::create('nutrition_plan_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('nutrition_id');
            $table->date('start_date');
            $table->date('end_date');
            $table->enum('week_start', config('dateTimeHelper.week_days'));
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exercises_plan_settings');
        Schema::dropIfExists('nutrition_plan_settings');
    }
}
