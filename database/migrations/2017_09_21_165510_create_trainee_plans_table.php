<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTraineePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trainee_plans', function (Blueprint $table) {
            $table->integer('plan_id')->unsigned();
            $table->integer('trainee_id')->unsigned();

            $table->foreign('plan_id')
                ->references('id')
                ->on('training_exercises')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('trainee_id')
                ->references('id')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->primary(['plan_id','trainee_id']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trainee_plans');
    }
}
