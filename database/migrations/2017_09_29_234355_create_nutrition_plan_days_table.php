<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNutritionPlanDaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nutrition_plan_days', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('day',config('dateTimeHelper.week_days'));
            $table->unsignedInteger('nutrition_plan_id');
            $table->foreign('nutrition_plan_id')->references('id')->on('nutritions_plans')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nutrition_plan_days');
    }
}
