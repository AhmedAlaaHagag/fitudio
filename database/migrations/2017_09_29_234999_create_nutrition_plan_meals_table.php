<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNutritionPlanMealsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nutrition_plan_meals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->enum('type', ['food', 'supplement']);
            $table->string('number_of_serving')->nullable();
            $table->string('size')->nullable();
            $table->string('NDB_No')->nullable();
            $table->boolean('is_free')->default(false);
            $table->unsignedInteger('supplement_id')->nullable();
            $table->unsignedInteger('day_id');

            $table->foreign('day_id')
                ->references('id')->on('nutrition_plan_days')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('supplement_id')
                ->references('id')->on('supplements')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('NDB_No')
                ->references('NDB_No')->on('nutrition_facts')
                ->onUpdate('cascade')
                ->onDelete('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nutrition_plan_meals');
    }
}
