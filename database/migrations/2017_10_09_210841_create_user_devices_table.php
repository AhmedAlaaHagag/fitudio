<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
// V2 ISA
//        Schema::create('user_devices', function (Blueprint $table) {
//            $table->increments('id');
//            $table->enum('device_type', ['android', 'ios']);
//            $table->unsignedInteger('user_id');
//            $table->string('device_token');
//
//            $table->foreign('user_id')
//                ->references('id')->on('users')
//                ->onUpdate('cascade')
//                ->onDelete('cascade');
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_devices');
    }
}
