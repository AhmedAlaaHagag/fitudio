<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTrainerToPlans extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('training_exercises', function (Blueprint $table) {
			if (!Schema::hasColumn('training_exercises', 'trainer_id')) {
				$table->unsignedInteger('trainer_id')->nullable()->default(null);
				$table->foreign('trainer_id')->references('id')->on('users')->onDelete('cascade');
			}
		});
		Schema::table('nutritions_plans', function (Blueprint $table) {
			if (!Schema::hasColumn('nutritions_plans', 'trainer_id')) {
				$table->unsignedInteger('trainer_id')->nullable()->default(null);
				$table->foreign('trainer_id')->references('id')->on('users')->onDelete('cascade');
			}
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
        Schema::disableForeignKeyConstraints();
		Schema::table('nutritions_plans', function (Blueprint $table) {
            $table->dropForeign('nutritions_plans_trainer_id_foreign');
            $table->dropColumn('trainer_id');
		});
		Schema::table('training_exercises', function (Blueprint $table) {
			$table->dropForeign('training_exercises_trainer_id_foreign');
			$table->dropColumn('trainer_id');
		});
        Schema::enableForeignKeyConstraints();
	}
}
