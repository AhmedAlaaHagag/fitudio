<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPlanableToTraineeTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::dropIfExists('trainee_plans');
		Schema::create('trainee_plans', function (Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('trainee_id');
			$table->foreign('trainee_id')
				->references('id')->on('users')
				->onUpdate('cascade')
				->onDelete('cascade');
			$table->unsignedInteger('planable_id')->nullable()->default(null);
			$table->string('planable_type')->nullable()->default(null);
			$table->timestamps();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('trainee_plans');
	}
}
