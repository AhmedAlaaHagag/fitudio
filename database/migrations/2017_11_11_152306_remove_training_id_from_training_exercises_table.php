<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveTrainingIdFromTrainingExercisesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('training_exercises', function (Blueprint $table) {
            DB::statement('ALTER TABLE `training_exercises` MODIFY `training_id` INTEGER UNSIGNED NULL;');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('training_exercises', function (Blueprint $table) {
            DB::statement('ALTER TABLE `training_exercises` MODIFY `training_id` INTEGER UNSIGNED;');
        });
    }
}
