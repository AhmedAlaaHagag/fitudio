<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ArticlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('articles')->insert([
          [
            'title' => 'Article 1',
            'image' => 'https://upload.wikimedia.org/wikipedia/commons/thumb/1/1e/Orlen_Warsaw_Marathon_2014_al._KEN.JPG/300px-Orlen_Warsaw_Marathon_2014_al._KEN.JPG',
            'details' => 'The marathon is a long-distance running race with an official distance of 42.195 kilometres (26.219 miles, or 26 miles 385 yards),[1] usually run as a road race. The event was instituted in commemoration of the fabled run of the Greek soldier Pheidippides, a messenger from the Battle of Marathon to Athens, who reported the victory.',
            'created_by' => 1,
            'created_at' => new Carbon(),
            'updated_at' => new Carbon(),
          ],
          [
              'title' => 'Article 2',
              'image' => 'http://www.asics.com/medias2/SPEED-255x185.jpg',
              'details' => 'Variation in training helps runners to stretch their limits and achieve their goals so whether you plan to run a marathon or just a 5K in the park, ASICS offers you the best running gear to help you push your boundaries. Train your body in multiple ways by mixing your run with road running, speed running, natural running and trail running. Whatever run you choose, make sure you are prepared with the right shoes, clothing and accessories.',
              'created_by' => 1,
              'created_at' => new Carbon(),
              'updated_at' => new Carbon(),
          ],
          [

              'title' => 'Article 3',
              'image' => 'http://crystallowery.com/wp-content/uploads/2015/03/soccer_nutrition.jpg',
              'details' => 'Variation in training helps runners to stretch their limits and achieve their goals so whether you plan to run a marathon or just a 5K in the park, ASICS offers you the best running gear to help you push your boundaries. Train your body in multiple ways by mixing your run with road running, speed running, natural running and trail running. Whatever run you choose, make sure you are prepared with the right shoes, clothing and accessories.',
              'created_by' => 1,
              'created_at' => new Carbon(),
              'updated_at' => new Carbon(),
          ]
        ]);
    }
}
