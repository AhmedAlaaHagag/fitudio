<?php

use Illuminate\Database\Seeder;
use App\Speciality;

class DatabaseSeeder extends Seeder
{
    protected function csvToArray($filename = 'nutrition_facts.csv', $delimiter = ',')
    {
      if(!file_exists($filename) || !is_readable($filename)) return false;

      $header = null;
      $data = array();
      if(($handle = fopen($filename, 'r')) !== false)
      {
        while(($row = fgetcsv($handle, 100000, $delimiter)) !== false)
        {
          if(!$header) $header = $row;
          else $data[] = array_combine($header, $row);
        }
        fclose($handle);
      }
      return $data;
    }

    protected function importcsv()
    {
      $file = base_path('/public/files/nutrition_facts.csv');

      $nutritionArr = $this->csvToArray($file);

      $total = count($nutritionArr); $div = 790; $input = [];
      for($i = 0; $i < $total; ++$i)
      {
        if($i == $div){
          DB::table('nutrition_facts')->insert($input);
          $input = [];
          $div += 1000;
        }
        array_push($input, $nutritionArr[$i]);
      }
      DB::table('nutrition_facts')->insert($input);
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);


        // seeding to specialities table
        DB::table('specialities')->insert([
          ['speciality' => 'Body Toning'],
          ['speciality' => 'Yoga'],
          ['speciality' => 'Boxing'],
          ['speciality' => 'Boot Camp'],
          ['speciality' => 'CrossFit'],
          ['speciality' => 'Lagree'],
          ['speciality' => 'Marathon/Traithlon'],
          ['speciality' => 'Mobility'],
          ['speciality' => 'Pilates'],
          ['speciality' => 'Strength and Conditiong'],
          ['speciality' => 'Weightlifting']
        ]);
        $this->call(SupplementsTableSeeder::class);
        $this->call(SupplementsTypesTableSeeder::class);
        echo "seeding 8970 records into nutrition_facts table\n";
        $this->importcsv();
    }
}
