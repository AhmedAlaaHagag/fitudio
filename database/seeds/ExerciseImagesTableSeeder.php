<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\User;

class ExerciseImagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $exercises = \App\Exercise::get();
        foreach ($exercises as $exercise){
            $image = new \App\Image();
            $image['image_url']='http://34.204.107.84/public/images/exercises/'.$exercise->name.'1.PNG';
            $image['image_name']= $exercise->name.'1.PNG';
            $image['imageable_id']= $exercise->id;
            $image['imageable_type']= 'App\\Exercise';
            $image->save();
            $image = new \App\Image();
            $image['image_url']='http://34.204.107.84/public/images/exercises/'.$exercise->name.'2.PNG';
            $image['image_name']= $exercise->name.'2.PNG';
            $image['imageable_id']= $exercise->id;
            $image['imageable_type']= 'App\\Exercise';
            $image->save();
        }
    }
}
