<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Maatwebsite\Excel\Facades\Excel;

class ExercisesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Excel::load('database/imports/Exercises.xlsx', function ($reader) {
            $results = $reader->get()->toArray();
            foreach ($results as $result) {
                $exercises = new \App\Exercise();
                $exercises->name = $result['exercise_name'];
                $exercises->target_area = $result['target_area'];
                $exercises->muscle_id = \App\Muscle::where('name', $result['target_muscle'])->first()->id;
                $exercises->exercise_type_id = \App\ExerciseType::firstOrCreate(['name' =>$result['type_of_exercise']])->id;
                $exercises->equipment_needed = $result['equipment_needed'];
                $exercises->level = $result['level'];
                $exercises->alternative_name = $result['aka'];
                $exercises->created_at = new Carbon();
                $exercises->updated_at = new Carbon();
                $exercises->save();
            }
        });
    }
}
