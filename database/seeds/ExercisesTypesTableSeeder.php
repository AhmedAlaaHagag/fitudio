<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class ExercisesTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('exercises_types')->insert(
            [['name' => 'Cardio',
                'created_at' => new Carbon(),
                'updated_at' => new Carbon()],
                ['name' => 'Olympic Weightlifting',
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon()],
                ['name' => 'Plyometrics',
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon()],
                ['name' => 'Powerlifting',
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon()],
                ['name' => 'Strength',
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon()],
                ['name' => 'Stretching',
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon()],
                ['name' => 'Strongman',
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon()]
            ]);

    }
}
