<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class MusclesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('muscles')->insert(
            [['name' => 'Abductors',
                'created_at' => new Carbon(),
                'updated_at' => new Carbon()],
                ['name' => 'Glutes',
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon()],
                ['name' => 'Triceps',
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon()],
                ['name' => 'Traps',
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon()],
                ['name' => 'Shoulders',
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon()],
                ['name' => 'Quadriceps',
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon()],
                ['name' => 'Neck',
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon()],
                ['name' => 'Middle Back',
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon()],
                ['name' => 'Lower Back',
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon()],
                ['name' => 'Lats',
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon()],
                ['name' => 'Hamstrings',
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon()],
                ['name' => 'Forearms',
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon()],
                ['name' => 'Chest',
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon()],
                ['name' => 'Calves',
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon()],
                ['name' => 'Biceps',
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon()],
                ['name' => 'Adductors',
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon()],
                ['name' => 'Abdominals',
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon()]]);

    }
}
