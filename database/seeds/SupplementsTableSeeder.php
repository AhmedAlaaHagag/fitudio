<?php

use Illuminate\Database\Seeder;
use Maatwebsite\Excel\Facades\Excel;
use App\SupplementType;


class SupplementsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Excel::load('database/imports/Supplements.xlsx', function ($reader) {
            $results = $reader->get()->toArray();


            try {
                foreach ($results as $result) {
                    $name = trim($result['supplement_name']);
                    $description = trim($result['description']);
                    if ($name && $description) {
                        $type = \App\SupplementType::firstOrCreate(['name' => $result['category']]);
                        $supplement = new \App\Supplement();
                        $supplement->name = $name;
                        $supplement->description = $description;
                        $supplement->supplement_type_id = $type->id;
                        $supplement->save();
                    }

                }
            } catch (\Exception $e) {
                echo $e->getMessage();
            }
        });
    }
}
