<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class SupplementsTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('supplements_types')->insert(
            [['name' => 'Testosterone Boosters',
                'created_at' => new Carbon(),
                'updated_at' => new Carbon()],
                ['name' => 'Weight-Gainers',
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon()],
                ['name' => 'Post-Workouts',
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon()],
                ['name' => 'Pre-Workouts',
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon()],
                ['name' => 'Protein Bars',
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon()],
                ['name' => 'Protein Powders',
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon()],
                ['name' => 'Amino Acids',
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon()],
                ['name' => 'CLA',
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon()],
                ['name' => 'Creatine',
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon()],
                ['name' => 'Fat Burners',
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon()],
                ['name' => 'Fish Oil',
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon()],
                ['name' => 'Intra-Workouts',
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon()],
                ['name' => 'Multivitamins',
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon()]
            ]);
    }
}
