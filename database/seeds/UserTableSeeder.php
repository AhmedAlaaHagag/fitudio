<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
          [
            'first_name' => 'John',
            'last_name' => 'Doe',
            'email' => 'johndoe@example.com',
            'phone' => '0123456789',
            'password' => app('hash')->make('johndoe'),
            'remember_token' => str_random(10),
            'type' => 'trainer',
            'city' => 'Alexandria',
            'lat' => 31.330904,
            'lng' => 30.0864017,
            'birth_date' => '1890-11-20',
            'activated' => 1,
            'created_at' => new Carbon(),
            'updated_at' => new Carbon(),
          ],
          [
            'first_name' => 'Mohaemd',
            'last_name' => 'Test',
            'email' => 'mohamed@gmail.com',
            'phone' => '01134567890',
            'password' => app('hash')->make('secret'),
            'remember_token' => str_random(10),
            'type' => 'trainee',
            'city' => 'Cairo',
            'birth_date' => '1994-11-29',
            'lat' => 30.0444196,
            'lng' => 31.2357116,
            'activated' => 1,
            'created_at' => new Carbon(),
            'updated_at' => new Carbon(),
          ],
          [
            'first_name' => 'Bakly',
            'last_name' => 'Bakly',
            'email' => 'bakly@gmail.com',
            'phone' => '0103456789',
            'password' => app('hash')->make('secret'),
            'remember_token' => str_random(10),
            'type' => 'trainee',
            'city' => 'Madrid',
            'birth_date' => '1700-2-15',
            'lat' => 40.4167754,
            'lng' => -3.7037902,
            'activated' => 1,
            'created_at' => new Carbon(),
            'updated_at' => new Carbon(),
          ]
        ]);
    }
}
