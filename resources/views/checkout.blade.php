<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Stripe</title>
    <style media="screen">
        * {
            font-family: monospace;
        }
        select {
            border-radius: 5px;
            height: 3em;
        }
        td {
            text-align: center;
        }
        pre {
            background-color: #D1CDCD;
            text-align: left;
            border-radius: 5px;
            max-width: 35rem;
            padding: 2px;
        }
        .var {
            text-align: center;
            background-color: #D1CDCD;
            color: #BD2016;
            border-radius: 5px;
            padding: 2px;
        }
        .container {
            /*margin: 0 40%;*/
        }
    </style>
  </head>
  <body>
    <div class="container">
      <h2>This is Stripe checkout demo for pending payments</h2>
      <form action="{{url('api/checkout-test')}}" method="post">
          <table>
              <tr>
                  <td>
                      <select name="model_id">
                          <option value="0">Select training to pay for</option>
                          @foreach ($trainings as $training)
                              <option value="{{$training->id}}">Training with coach {{$training->trainer->name}}</option>
                          @endforeach
                      </select>
                  </td>
              </tr>
              <tr>
                  <td>
                      <script src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                      data-key="{{env('STRIPE_PUBLISHABLE')}}"
                      data-description="Access for a year"
                      data-amount = "20000"
                      data-currency = "egp"
                      data-locale="auto"></script>
                  </td>
              </tr>
          </table>
          <input type="hidden" name="amount" value="20000">
          <input type="hidden" name="model_type" value="training">
      </form>
      {{-- instructions --}}
      <p>
          Send <span class="var">post</span> request to
          <a href="http://fitudio.baklysystems.com/api/checkout">
              http://fitudio.baklysystems.com/api/checkout</a> with
          <pre> sent data example
    {
      model_id: "1",
      model_type: "training",
      amount: "20000",
      stripeToken: "tok_1AZX5yFs29CKGmCN0ThEekCx",
      stripeTokenType: "card",
      stripeEmail: "example@mail.com"
    }</pre>
          The <span class="var">model_id</span> field is the <span class="var">id</span> of the training/event.
      </p>
      <p>
          The <span class="var">amount</span> field is set by frontend in Strip's script.
      </p>
      <p>
          Here is the form which sends the above to the server
          <pre>&lt;script src="https://checkout.stripe.com/checkout.js"
      class="stripe-button"
      data-key="{{env('STRIPE_PUBLISHABLE')}}"
      data-description="Access for a year"
      data-amount = "20000"
      data-currency = "egp"
      data-locale="auto"&gt;
&lt;/script&gt;
&lt;input type="hidden" name="amount" value="20000"&gt;
&lt;input type="hidden" name="model_type" value="training"&gt;</pre>
      </p>
      <p>
          API stores in DB for each record
          <pre>{
  trainer_id => trainer_id, <i>// NULL for events</i>
  amount => amount,
  email => stripeEmail,
  charge => charge->id,
  payable_id => model_id,
  payable_type => model_type
}</pre>
  </p>
    </div>
  </body>
</html>
