@extends('layouts.master')

@section('title')
Event Confirmation
@stop

@section('content')
<p>Thank you {{$user->name}} for registering in {{$event->title}}.</p>
@stop
