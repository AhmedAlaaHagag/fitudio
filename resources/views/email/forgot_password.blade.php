@extends('layouts.master')

@section('title')
Password Reset Request
@stop

@section('content')
<p>You have requested to reset your password please use this code <b>{{$token}}</b>.</p>
@stop
