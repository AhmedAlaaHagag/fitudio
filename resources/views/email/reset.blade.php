@extends('layouts.master')

@section('title')
Password Reset
@stop

@section('content')
<p>Your new password is {{$token}}.</p>
@stop
