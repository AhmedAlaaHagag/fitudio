<html>
<body style="width:520px;margin:auto;border:solid 1px #ccc;padding:10px;text-align:center;font-family:Helvetica;">
	<div style="width:100%;text-align:center;">
	<h1 style="color:#42BBB2;font:35px Helvetica,Arial,sans-serif;">FITUDIO</h1>	
	<img style="max-width:300px;text-align:center;" src="{{url('files/Img_Trainee.png')}}"/></div>
	<h1 style="font:bold 44px Helvetica,Arial,sans-serif;">COACH WITH YOUR FAVOURITE P.T ONLINE</h1>
	<p style="color:#8d8d8d;font:16px Helvetica,Arial,sans-serif">You've got the FITUDIO app, now it's time to get connected with your favorite personal trainer.
		It is the most reliable way for getting accurate results. Having a balance between nutrition and exercise plan that gets adjusted each month according to your progress, is what makes you reach your fitness goals.</p>
	<h1 style="font:bold 44px Helvetica,Arial,sans-serif;">HOW IT WORKS?</h1> 
	<p style="color:#8d8d8d;font:16px Helvetica,Arial,sans-serif"> 
		<div style="border: solid 1px #ccc; box-shadow: 0px 10px 6px -6px #777;min-height: 50px;vertical-align: middle;line-height: 50px;margin-bottom: 20px;font: normal 16px Roboto;color: #42BBB2;text-align: left;padding-left: 10px;padding-top: 7px;"><div style="border-radius: 30px;border: solid 1px #42BBB2;width: 35px; height: 35px;line-height: 37px; color: #42BBB2;text-align: center; display: inline-block; margin-right: 10px;">1</div>Book a training request from your personal trainer</div>
	 <div style="border: solid 1px #ccc; box-shadow: 0px 10px 6px -6px #777;min-height: 50px;vertical-align: middle;line-height: 50px;margin-bottom: 20px;font: normal 16px Roboto;color: #42BBB2;text-align: left;padding-left: 10px;padding-top: 7px;"><div style="border-radius: 30px;border: solid 1px #42BBB2;width: 35px; height: 35px;line-height: 37px; color: #42BBB2;text-align: center; display: inline-block; margin-right: 10px;">2</div>Wait for a confirmation on your training request from the trainer</div>
	 <div style="border: solid 1px #ccc; box-shadow: 0px 10px 6px -6px #777;min-height: 50px;vertical-align: middle;line-height: 50px;margin-bottom: 20px;font: normal 16px Roboto;color: #42BBB2;text-align: left;padding-left: 10px;padding-top: 7px;"><div style="border-radius: 30px;border: solid 1px #42BBB2;width: 35px; height: 35px;line-height: 37px; color: #42BBB2;text-align: center; display: inline-block; margin-right: 10px;">3</div>Make payment</div>
     <div style="border: solid 1px #ccc; box-shadow: 0px 10px 6px -6px #777;min-height: 50px;vertical-align: middle;line-height: 50px;margin-bottom: 20px;font: normal 16px Roboto;color: #42BBB2;text-align: left;padding-left: 10px;padding-top: 7px;"><div style="border-radius: 30px;border: solid 1px #42BBB2;width: 35px; height: 35px;line-height: 37px; color: #42BBB2;text-align: center; display: inline-block; margin-right: 10px;">4</div>Receive your nutrition and exercise plan each month</div>
 </p>
	
	
	
	
<p class="footer">	
	<a href="#">Privacy Policy</a> 
	<br/>
	Questions or concerns? <a href="mailto:info@fitudio.com">Contact us</a> 
	<br/>
	Please don’t reply directly to this email—we won’t see your message.
	<br/>
	<br/>
	© 2017 Fitudio, Inc. All Rights Reserved.
</p>
	
	
</body>
</html>