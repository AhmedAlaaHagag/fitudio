<html>
<style>
	a{cursor:pointer;}
</style>
<body style="width:520px;margin:auto;border:solid 1px #ccc;padding:10px;text-align:center;font-family:Helvetica;">
	<div style="width:100%;text-align:center;">
	<h1 style="color:#42BBB2;font:35px Helvetica,Arial,sans-serif;">FITUDIO</h1>	
	<img style="max-width:300px;text-align:center;" src="{{url('files/Img_Trainer.png')}}"/></div>
	<h1 style="font:bold 44px Helvetica,Arial,sans-serif;">COACH YOUR CLIENTS EASILY ONLINE</h1>
	<p style="color:#8d8d8d;font:16px Helvetica,Arial,sans-serif">You've got the FITUDIO app, now it's time to get connected with your clients.
		It is the most reliable way for accepting client requests, building nutrition and exercise plans in less than half an hour, sending the plans and receiving your payment into your bank account!</p>
	<h1 style="font:bold 44px Helvetica,Arial,sans-serif;">HOW IT WORKS?</h1> 
	<p style="color:#8d8d8d;font:16px Helvetica,Arial,sans-serif"> 
		<div style="border: solid 1px #ccc; box-shadow: 0px 10px 6px -6px #777;min-height: 50px;vertical-align: middle;line-height: 50px;margin-bottom: 20px;font: normal 16px Roboto;color: #42BBB2;text-align: left;padding-left: 10px;padding-top: 7px;"><div style="border-radius: 30px;border: solid 1px #42BBB2;width: 35px; height: 35px;line-height: 37px; color: #42BBB2;text-align: center; display: inline-block; margin-right: 10px;">1</div>Accept or decline training requests from clients</div>
	 <div style="border: solid 1px #ccc; box-shadow: 0px 10px 6px -6px #777;min-height: 50px;vertical-align: middle;line-height: 50px;margin-bottom: 20px;font: normal 16px Roboto;color: #42BBB2;text-align: left;padding-left: 10px;padding-top: 7px;"><div style="border-radius: 30px;border: solid 1px #42BBB2;width: 35px; height: 35px;line-height: 37px; color: #42BBB2;text-align: center; display: inline-block; margin-right: 10px;">2</div>Receive payment with only a 10% commission for Fitudio</div>
	 <div style="border: solid 1px #ccc; box-shadow: 0px 10px 6px -6px #777;min-height: 50px;vertical-align: middle;line-height: 50px;margin-bottom: 20px;font: normal 16px Roboto;color: #42BBB2;text-align: left;padding-left: 10px;padding-top: 7px;"><div style="border-radius: 30px;border: solid 1px #42BBB2;width: 35px; height: 35px;line-height: 37px; color: #42BBB2;text-align: center; display: inline-block; margin-right: 10px;">3</div>Create nutrition and exercise plans</div>
     <div style="border: solid 1px #ccc; box-shadow: 0px 10px 6px -6px #777;min-height: 50px;vertical-align: middle;line-height: 50px;margin-bottom: 20px;font: normal 16px Roboto;color: #42BBB2;text-align: left;padding-left: 10px;padding-top: 7px;"><div style="border-radius: 30px;border: solid 1px #42BBB2;width: 35px; height: 35px;line-height: 37px; color: #42BBB2;text-align: center; display: inline-block; margin-right: 10px;">4</div>Track your clients progress each month</div>
 </p>
	
	
	
	
<p class="footer">	
	<a href="#">Privacy Policy</a> 
	<br/>
	Questions or concerns? <a href="mailto:info@fitudio.com">Contact us</a> 
	<br/>
	Please don’t reply directly to this email—we won’t see your message.
	<br/>
	<br/>
	© 2017 Fitudio, Inc. All Rights Reserved.
</p>
	
	
</body>
</html>