@extends('layouts.master')

@section('title')
Email Verification
@stop

@section('content')
<p>
	Welcome to Fitudio! Please click below to confirm your new Fitudio account
	<br>
	<a href="{{url('/api/register/verfiy')}}/{{$activation_code}}">{{url('/api/register/verfiy')}}</a>
</p>
<p>
	The Fitudio Team
</p>
@stop
