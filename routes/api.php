<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$api = $app->make(Dingo\Api\Routing\Router::class);

$api->version('v1', function ($api) {
    $api->post('/auth/login', [
        'as' => 'api.auth.login',
        'uses' => 'App\Http\Controllers\AuthController@login',
    ]);

     $api->post('/auth/social/login', [
         'uses' => 'App\Http\Controllers\Auth\AuthController@socialLogin'
    ]);


    resource('specialities', 'SpecialitiesController', $api); // specialities

    $api->group([
        'middleware' => 'api.auth',
    ], function ($api) {
        $api->post('update-token','App\Http\Controllers\UsersController@updateToken');
        $api->get('/', [
            'uses' => 'App\Http\Controllers\APIController@getIndex',
            'as' => 'api.index'
        ]);
        $api->get('/auth/user', [
            'uses' => 'App\Http\Controllers\Auth\AuthController@getUser',
            'as' => 'api.auth.user'
        ]);
        $api->patch('/auth/refresh', [
            'uses' => 'App\Http\Controllers\Auth\AuthController@patchRefresh',
            'as' => 'api.auth.refresh'
        ]);
        $api->delete('/auth/invalidate', [
            'uses' => 'App\Http\Controllers\Auth\AuthController@deleteInvalidate',
            'as' => 'api.auth.invalidate'
        ]);

        // users
        // resource('users', 'UsersController',$api); // no longer valid
        $api->get('users', 'App\Http\Controllers\UsersController@index');
        $api->get('users/{user_id}', 'App\Http\Controllers\UsersController@show');
        $api->put('users/{user_id}', 'App\Http\Controllers\UsersController@update');
        $api->delete('users/{user_id}', 'App\Http\Controllers\UsersController@destroy');
        $api->post('users/reset', 'App\Http\Controllers\UsersController@reset');

        // exercies and nutirations
        $api->get('plans/{type}/{id}', 'App\Http\Controllers\PlansController@getPlan');
        $api->post('plans/{type}','App\Http\Controllers\PlansController@store');
        $api->get('exercise-plans/day/{day}', 'App\Http\Controllers\ExercisePlanController@getByDay');
        $api->post('plan','App\Http\Controllers\ExercisePlanController@store');
        $api->post('exercise-plans-setting','App\Http\Controllers\ExercisePlanController@exercisePlansSetting');
        $api->post('send-plan','App\Http\Controllers\ExercisePlanController@sendPlan');

        // Nutritions
        $api->get('nutrition/{id}', 'App\Http\Controllers\NutritionController@getNutrition');
        $api->get('foods', 'App\Http\Controllers\NutritionController@getFoods');
        $api->get('supplements', 'App\Http\Controllers\NutritionController@getSupplements');

        /* routes to be changed to fit the naming conventions */
        resource('rates', 'RateController', $api); // rates
        resource('events', 'EventController', $api); // events
        resource('follows', 'FollowController', $api); // follows
        resource('settings', 'SettingsController', $api); // settings
        resource('medicals', 'MedicalsController', $api); // medical issues
        resource('payments', 'PaymentsController', $api); // payments
        resource('trainings', 'TrainingController', $api); // trainings
        resource('exercises', 'ExerciseController', $api); // exercises
        resource('nutritions', 'NutritionController', $api); // nutritions
        resource('data-points', 'DataPointController', $api); // data points
        resource('notifications', 'NotificationsController', $api); // notifications
        resource('exercise-plans', 'ExercisePlanController', $api); // exercise plans
        resource('trainers-work-options', 'TrainerController', $api); // trainer work options
        resource('trainer-availability', 'TrainerScheduleController', $api); // set trainer availability
        resource('articles', 'ArticlesController', $api); // articles
        resource('home', 'HomeController', $api); // articles
        resource('muscles', 'MuscleController', $api); // muscle

        // tainers and trainees
        $api->get('trainer/home/{id}', ['uses' => 'App\Http\Controllers\Trainers\HomeController@index']);
        $api->get('trainer/{id}/requests', ['uses' => 'App\Http\Controllers\Trainers\HomeController@index']);
        $api->get('plans/{id}', ['uses' => 'App\Http\Controllers\PlansController@show']);
        $api->post('edit-trainee/{id}', ['uses' => 'App\Http\Controllers\TraineeController@updateTrainee']);

        // trainee
        $api->get('trainee-details/{id}', ['uses' => 'App\Http\Controllers\TraineeController@show']);
        $api->get('trainee-dashboard/{id}', ['uses' => 'App\Http\Controllers\TraineeController@traineeDashboard']);
        $api->put('edit-trainee/{id}', ['uses' => 'App\Http\Controllers\TraineeController@updateTrainee']);
                
	// tainers and trainees
        $api->post('edit-trainer/{id}', ['uses' => 'App\Http\Controllers\UsersController@update']);
        $api->post('upload-trainer-image/{user-id}', 'App\Http\Controllers\ImageController@store');
        $api->post('upload-trainee-image/{user-id}', 'App\Http\Controllers\ImageController@store');
        $api->delete('delete-image/{id}', 'App\Http\Controllers\ImageController@destroy');
        // list nearby trainers
        $api->get('nearby-trainers', 'App\Http\Controllers\TrainerController@nearby');
        // trainers specialities
        $api->post('create-speciality', 'App\Http\Controllers\TrainerSpecialityController@store');
        $api->delete('delete-speciality/{user_id}/{speciality_id}', 'App\Http\Controllers\TrainerSpecialityController@destroy');
        // event images
        $api->post('upload-event-img/{event-id}', 'App\Http\Controllers\EventController@upload_image');
        $api->delete('delete-event-img/{event_id}', 'App\Http\Controllers\EventController@delete_image');
        // event registration
        $api->post('register-event', 'App\Http\Controllers\EventController@register_event');
        $api->post('cancel-event', 'App\Http\Controllers\EventController@cancel_event');
        $api->post('events/{id}', 'App\Http\Controllers\EventController@update');
        // nearby evnets
        $api->get('nearby-events', 'App\Http\Controllers\EventController@near_by');
        // trainers listing
        $api->get('trainer-details/{trainer_id}', 'App\Http\Controllers\TrainerController@show');
        $api->get('trainers-list', 'App\Http\Controllers\TrainerController@index');
        $api->get('trainer-work-options/{trainer_id}', 'App\Http\Controllers\TrainerController@TrainerWorkOptions');
        $api->delete('trainer-work-options', 'App\Http\Controllers\TrainerController@destroy');
        // training request and response
        $api->get('delete-training/{training_id}', 'App\Http\Controllers\TrainingController@destroy');
        $api->post('request-training', 'App\Http\Controllers\TrainingController@request');
        $api->post('response-training', 'App\Http\Controllers\TrainingController@response');
        $api->post('trainer-clients/{trainer_id}', 'App\Http\Controllers\TrainerController@trainerClients');
        $api->get('pending-trainings/{id}', 'App\Http\Controllers\TrainerController@pendingRequests');
        $api->get('pending-payments/{id}', 'App\Http\Controllers\TrainingController@pending');
        // device id token
        $api->post('set-device-token', 'App\Http\Controllers\UsersController@deviceToken');
        // update exercise
        $api->post('exercises/{id}', 'App\Http\Controllers\ExerciseController@update');
        $api->get('exercise/muscle/{id}', 'App\Http\Controllers\ExerciseController@getExerciseByMusclesGroups');
        $api->post('exercise-image/{id}', 'App\Http\Controllers\ExerciseController@exerciseImage');
        $api->delete('exercise-image/{image_id}', 'App\Http\Controllers\ExerciseController@deleteImage');
        // stripe
        $api->post('checkout', 'App\Http\Controllers\StripeController@checkout');
        $api->post('refund', 'App\Http\Controllers\StripeController@refund');
        // payments
        $api->post('trainer/{id}/earnings', 'App\Http\Controllers\TrainerController@earnings');
        // rates
        $api->get('trainer/{id}/rate', 'App\Http\Controllers\RateController@show');
        // earnings

        // medical histories
        $api->get('medicals-enums', 'App\Http\Controllers\MedicalsController@enums');
        // Notifications
        $api->get('notifications-count/{user_id}', 'App\Http\Controllers\NotificationsController@count');



    });
    // stripe for test
    $api->get('checkout-test/{id}', 'App\Http\Controllers\StripeController@show');
    $api->post('checkout-test', 'App\Http\Controllers\StripeController@checkout');

    // users registration
    $api->post('users', 'App\Http\Controllers\UsersController@store');
    $api->get('register/verfiy/{activation_code}', 'App\Http\Controllers\UsersController@verify');
    $api->post('users/reactivate', 'App\Http\Controllers\UsersController@reactivate');
    $api->post('users/forgot-password', 'App\Http\Controllers\UsersController@forgotPassword');
    $api->post('users/update-password', 'App\Http\Controllers\UsersController@updatePassword');
});


function resource($uri, $controller, $api)
{
    //$verbs = array('GET', 'HEAD', 'POST', 'PUT', 'PATCH', 'DELETE');
    $controller= 'App\Http\Controllers\\'.$controller;
    $api->get($uri, $controller.'@index');
    $api->get($uri.'/create', $controller.'@create');
    $api->post($uri, $controller.'@store');
    $api->get($uri.'/{id}', $controller.'@show');
    $api->get($uri.'/{id}/edit', $controller.'@edit');
    $api->put($uri.'/{id}', $controller.'@update');
    $api->patch($uri.'/{id}', $controller.'@update');
    $api->delete($uri.'/{id}', $controller.'@destroy');
}
